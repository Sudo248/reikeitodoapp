package com.sudo248.todoapp.ui.activity.main.fragment

object EventKeys {
    const val EVENT_DELETE_TASK = "EVENT_DELETE_TASK"
    const val EVENT_UPDATE_TASK = "EVENT_UPDATE_TASK"
}