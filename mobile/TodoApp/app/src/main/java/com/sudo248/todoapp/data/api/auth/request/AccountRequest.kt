package com.sudo248.todoapp.data.api.auth.request

data class AccountRequest(
    val email: String,
    val password: String,
    val agreePolicy: Boolean = true
)
