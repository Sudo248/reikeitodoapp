package com.sudo248.todoapp.domain.entity.task

import java.io.Serializable

enum class TaskStatus {
    PROCESSING,
    DONE,
    NOT_DOING
}