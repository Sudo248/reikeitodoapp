package com.sudo248.todoapp.ui.activity.main

import com.sudo248.base_android.ktx.createActionIntentDirections
import com.sudo248.base_android.ktx.onSuccess
import com.sudo248.base_android.navigation.IntentDirections
import com.sudo248.todoapp.domain.repository.AuthRepository
import com.sudo248.todoapp.ui.activity.auth.AuthActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val authRepository: AuthRepository
) : BaseAppViewModel<IntentDirections>() {

    fun logout() = bindUiState{
        authRepository.logout()
            .onSuccess {
                navigator.navigateOff(AuthActivity::class.createActionIntentDirections())
            }
    }
}