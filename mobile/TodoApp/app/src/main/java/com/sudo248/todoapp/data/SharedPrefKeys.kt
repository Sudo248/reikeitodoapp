package com.sudo248.todoapp.data

object SharedPrefKeys {
    const val TOKEN = "TOKEN"
    const val TASK_SORT_CONDITION = "TASK_SORT_CONDITION"
}