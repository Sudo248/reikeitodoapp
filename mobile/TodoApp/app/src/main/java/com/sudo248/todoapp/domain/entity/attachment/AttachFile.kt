package com.sudo248.todoapp.domain.entity.attachment

import com.google.gson.annotations.SerializedName
import com.sudo248.base_android.base.ItemDiff

data class AttachFile(
    @SerializedName("id") val attachFileId: String? = null,
    val url: String,
    val name: String,
) : ItemDiff {
    override fun isContentTheSame(other: ItemDiff): Boolean {
        return other is AttachFile && this == other
    }

    override fun isItemTheSame(other: ItemDiff): Boolean {
        return other is AttachFile &&
                this.attachFileId == other.attachFileId &&
                this.url == other.url &&
                this.name == other.name
    }

}