package com.sudo248.todoapp.ui.activity.main.fragment.taskdetail

import android.app.AlarmManager
import android.app.Dialog
import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.sudo248.base_android.ktx.onSuccess
import com.sudo248.base_android.ktx.showWithTransparentBackground
import com.sudo248.base_android.utils.DialogUtils
import com.sudo248.todoapp.R
import com.sudo248.todoapp.databinding.FragmentPreviewImageBinding
import com.sudo248.todoapp.domain.Constants
import com.sudo248.todoapp.domain.entity.attachment.AttachFile
import com.sudo248.todoapp.domain.entity.attachment.AttachmentAction
import com.sudo248.todoapp.domain.entity.category.Category
import com.sudo248.todoapp.domain.entity.task.Task
import com.sudo248.todoapp.domain.entity.task.TaskStatus
import com.sudo248.todoapp.domain.repository.AttachmentRepository
import com.sudo248.todoapp.domain.repository.CategoryRepository
import com.sudo248.todoapp.domain.repository.TaskRepository
import com.sudo248.todoapp.receiver.AlarmReceiver
import com.sudo248.todoapp.ui.activity.main.BaseAppViewModel
import com.sudo248.todoapp.ui.activity.main.builder.PopupMenuBuilder
import com.sudo248.todoapp.ui.activity.main.fragment.DataKeys
import com.sudo248.todoapp.ui.activity.main.fragment.NavigationKeys
import dagger.hilt.android.lifecycle.HiltViewModel
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@HiltViewModel
class TaskDetailViewModel @Inject constructor(
    private val taskRepository: TaskRepository,
    private val categoryRepository: CategoryRepository,
    private val attachmentRepository: AttachmentRepository
) : BaseAppViewModel<NavDirections>() {

    val textCategory = ObservableField<String>()
    val textTaskTitle = ObservableField<String>()
    val textTaskDeadline = ObservableField<String>()
    val textTaskAlert = ObservableField<String>()
    val textTaskNote = ObservableField<String>()

    private lateinit var currentTask: Task

    private val formatDate = DateTimeFormatter.ofPattern("dd/MM/yyyy")
    private val formatTime = DateTimeFormatter.ofPattern("HH:mm")

    private val _categories = MutableLiveData<List<Category>>()
    val categories: LiveData<List<Category>> = _categories

    private val _attachments = MutableLiveData<List<AttachFile>>()
    val attachments: LiveData<List<AttachFile>> = _attachments

    private var needCancelSchedule = false

    private val controller: TaskDetailViewController
        get() = viewController()

    private var isUpdateTask = false

    private fun setTask(task: Task) {
        currentTask = task
        textTaskTitle.set(task.title)
        textTaskDeadline.set(task.deadline.format(formatDate))
        textTaskAlert.set(task.timeAlert?.format(formatTime) ?: controller.getString(R.string.not))
        textTaskNote.set(task.note)
        _attachments.postValue(task.attachments.orEmpty())
    }

    fun getTask(taskId: Int) = bindUiState {
        taskRepository.getTaskById(taskId)
            .onSuccess {
                needCancelSchedule = !this::currentTask.isInitialized && it.timeAlert != null
                setTask(it)
            }
    }

    fun getCategories() = bindUiState {
        categoryRepository.getAllCategories(needCategoryAll = true)
            .onSuccess {
                val categories = it.toMutableList()
                categories[0] = categories[0].copy(
                    name = controller.getString(R.string.no_type)
                )
                _categories.postValue(categories)
            }
    }

    fun showPopupMenuTask(view: View) {
        PopupMenuBuilder(view)
            .setMenu(R.menu.popup_menu_task)
            .applyItemAt(0) {
                setTitle(
                    if (currentTask.status == TaskStatus.DONE) {
                        R.string.redo
                    } else {
                        R.string.make_done
                    }
                )
            }
            .setOnItemMenuClickListener {
                onItemMenuTask(view, it)
            }
            .build()
            .show()
    }

    private fun onItemMenuTask(view: View, item: MenuItem) {
        when (item.itemId) {
            R.id.makeDone -> {
                val task =
                    currentTask.copy(
                        status = if (currentTask.status == TaskStatus.DONE)
                            TaskStatus.PROCESSING
                        else
                            TaskStatus.DONE
                    )
                bindUiState {
                    taskRepository.updateTasks(task)
                        .onSuccess {
                            isUpdateTask = true
                            setTask(task)
                        }
                }
            }

            R.id.delete -> {
                showPopupConfirmDelete(
                    view,
                    controller.getString(R.string.delete_task, currentTask.title)
                ) {
                    bindUiState {
                        taskRepository.deleteTask(currentTask.taskId!!)
                            .onSuccess {
                                val data = Bundle().apply {
                                    putBoolean(DataKeys.IS_DELETE, true)
                                }
                                navigator.back(NavigationKeys.ACTION_TO_DETAIL_FRAGMENT, data)
                            }
                    }
                }
            }

            else -> {

            }
        }
    }

    fun showDatePicker() {
        controller.showDatePicker(currentTask.deadline) {
            setTask(currentTask.copy(deadline = it))
        }
    }

    fun showTimePicker() {
        controller.showTimePicker(currentTask.timeAlert) {
            setTask(currentTask.copy(timeAlert = it))
        }
    }

    fun onClickUpdateNote(view: View) {
        val dialog = Dialog(view.context)
        val dialogView =
            LayoutInflater.from(view.context)
                .inflate(R.layout.dialog_input_note, view.parent as? ViewGroup, false)
        dialog.setContentView(dialogView)

        dialogView.apply {
            findViewById<TextView>(R.id.txtTitle).text = controller.getString(R.string.note)
            val edtInput = findViewById<EditText>(R.id.edtInput)
            edtInput.setText(currentTask.note)
            findViewById<TextView>(R.id.txtNegative).setOnClickListener {
                dialog.hide()
            }
            findViewById<TextView>(R.id.txtPositive).setOnClickListener {
                if (currentTask.note != edtInput.text.toString()) {
                    setTask(currentTask.copy(note = edtInput.text.toString()))
                }

                dialog.hide()
            }
        }
        dialog.showWithTransparentBackground()
    }

    fun onClickAddAttachFile() {
        controller.pickAttachment()
    }

    fun onClickUpdateTask() = bindUiState {
        taskRepository.updateTasks(
            currentTask.copy(
                categoryId = getCategoryIdByName(textCategory.get()),
                title = textTaskTitle.get().orEmpty()
            )
        )
            .onSuccess {
                isUpdateTask = true
                scheduleTimeAlertIfNeeded(it)
                setTask(it)
            }
    }

    private fun getCategoryIdByName(name: String?): Int? {
        if (name == null || _categories.value.isNullOrEmpty()) return null
        return _categories.value?.firstOrNull { it.name == name }?.categoryId
    }

    private fun showPopupConfirmDelete(view: View, title: String, action: () -> Unit) {
        DialogUtils.showConfirmDialog(
            context = view.context,
            title = title,
            description = "",
            negative = controller.getString(R.string.cancel),
            positive = controller.getString(R.string.delete),
            onPositive = action
        )
    }

    fun onClickAttachmentItem(view: View, item: AttachFile, action: AttachmentAction) {
        when (action) {
            AttachmentAction.ACTION_DELETE_ITEM -> {
                showPopupConfirmDelete(
                    view,
                    controller.getString(R.string.delete_attachment, item.name)
                ) {
                    deleteAttachment(item)
                }
            }

            AttachmentAction.ACTION_CLICK_ITEM -> {
                navigator.navigateTo(
                    TaskDetailFragmentDirections.actionTaskDetailFragmentToPreviewImage(
                        item.url
                    )
                )
            }
        }
    }

    fun uploadFile(filePath: String) = bindUiState {
        attachmentRepository.uploadAttachment(currentTask.taskId!!, filePath)
            .onSuccess {
                val currentAttachment = currentTask.attachments.orEmpty().toMutableList()
                currentAttachment.add(it)
                setTask(task = currentTask.copy(attachments = currentAttachment))
            }
    }

    private fun deleteAttachment(file: AttachFile) = bindUiState {
        attachmentRepository.deleteAttachment(file.attachFileId!!)
            .onSuccess {
                val currentAttachment = currentTask.attachments.orEmpty().toMutableList()
                currentAttachment.removeIf { it.attachFileId == file.attachFileId }
                setTask(currentTask.copy(attachments = currentAttachment))
            }
    }

    private fun scheduleTimeAlertIfNeeded(task: Task) {
        if (task.timeAlert != null && !task.deadline.isBefore(LocalDate.now())) {
            if (needCancelSchedule) cancelSchedule()
            schedule(
                LocalDateTime.of(task.deadline, task.timeAlert).atZone(ZoneId.systemDefault())
                    .toInstant().toEpochMilli()
            )
        }
    }

    private fun schedule(time: Long) {
        val intent = controller.getIntent(AlarmReceiver::class.java).apply {
            putExtra(Constants.KEY_TITLE_NOTIFICATION, controller.getString(R.string.remid))
            putExtra(
                Constants.KEY_CONTENT_NOTIFICATION,
                controller.getString(
                    R.string.remid_content,
                    currentTask.title,
                    currentTask.deadline.format(formatDate)
                )
            )
        }
        val pendingIntent = controller.getBroadcast(
            currentTask.taskId ?: Constants.NOTIFICATION_ID,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        controller.getAlarmManager().set(AlarmManager.RTC_WAKEUP, time, pendingIntent)
    }

    private fun cancelSchedule() {
        val pendingIntent = controller.getBroadcast(
            currentTask.taskId ?: Constants.NOTIFICATION_ID,
            controller.getIntent(AlarmReceiver::class.java),
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        controller.getAlarmManager().cancel(pendingIntent)
    }

    fun back() {
        val data = Bundle().apply {
            putBoolean(DataKeys.IS_UPDATE, isUpdateTask)
            if (isUpdateTask && this@TaskDetailViewModel::currentTask.isInitialized) {
                putSerializable(DataKeys.TASK, currentTask)
            }
        }
        navigator.back(NavigationKeys.ACTION_TO_DETAIL_FRAGMENT, data)
    }

}