package com.sudo248.todoapp.domain.repository

import android.provider.ContactsContract.Data
import com.sudo248.base_android.core.DataState
import com.sudo248.todoapp.data.api.auth.request.AccountRequest
import com.sudo248.todoapp.data.api.auth.request.OtpRequest
import com.sudo248.todoapp.domain.entity.auth.Profile

interface AuthRepository {
    suspend fun login(accountRequest: AccountRequest): DataState<Unit, Exception>
    suspend fun signUp(accountRequest: AccountRequest): DataState<Unit, Exception>
    suspend fun confirmOtp(otpRequest: OtpRequest): DataState<Unit, Exception>

    suspend fun logout(): DataState<Unit, Exception>
}