package com.sudo248.todoapp.ui.activity.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.GravityCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.customview.widget.Openable
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.sudo248.base_android.base.BaseActivity
import com.sudo248.base_android.ktx.gone
import com.sudo248.base_android.ktx.visible
import com.sudo248.todoapp.R
import com.sudo248.todoapp.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {
    override val viewModel: MainViewModel by viewModels()

    private lateinit var navController: NavController
    private var savedInstanceState: Bundle? = null

    private val listFragmentHideBottomNav = listOf(
        R.id.taskDetailFragment,
        R.id.listCategoryFragment,
        R.id.previewImage,
    )

    private val listener = NavController.OnDestinationChangedListener { _, destination, _ ->
        if (destination.id in listFragmentHideBottomNav) {
            goneBottomNav()
        } else {
            showBottomNav()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null) {
            this.savedInstanceState = savedInstanceState
        } else {
            this.savedInstanceState = null
        }
    }

    override fun initView() {
        val navHost = supportFragmentManager.findFragmentById(R.id.fcv) as NavHostFragment
        navController = navHost.navController
        binding.bottomNav.setupWithNavController(navController)
        binding.navView.setupWithNavController(navController)
        binding.navView.setNavigationItemSelectedListener { item ->
            if (item.itemId == R.id.logout) {
                viewModel.logout()
                true
            } else {
                val handled = NavigationUI.onNavDestinationSelected(item, navController)
                if (handled) {
                    val parent = binding.navView.parent
                    if (parent is Openable) {
                        parent.close()
                    } else {
                        val bottomSheetBehavior =
                            NavigationUI.findBottomSheetBehavior(binding.navView)
                        if (bottomSheetBehavior != null) {
                            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        }
                    }
                }
                handled
            }

        }
        binding.imgSetting.setOnClickListener {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            onBackPressedDispatcher.onBackPressed()
        }
    }

    private fun goneBottomNav() {
        if (binding.lnBottomnav.isVisible) {
            binding.lnBottomnav.gone()
        }
    }

    private fun showBottomNav() {
        if (binding.lnBottomnav.isGone) {
            binding.lnBottomnav.visible()
        }
    }

    override fun onResume() {
        super.onResume()
        navController.addOnDestinationChangedListener(listener)
    }

    override fun onPause() {
        super.onPause()
        navController.removeOnDestinationChangedListener(listener)
    }

}