package com.sudo248.todoapp.ui.activity.auth.fragment.signup

import androidx.databinding.ObservableField
import androidx.navigation.NavDirections
import com.sudo248.base_android.ktx.onSuccess
import com.sudo248.todoapp.data.api.auth.request.AccountRequest
import com.sudo248.todoapp.domain.repository.AuthRepository
import com.sudo248.todoapp.ui.activity.main.BaseAppViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val authRepository: AuthRepository
) : BaseAppViewModel<NavDirections>() {

    val edtEmail = ObservableField<String>()
    val edtPassword = ObservableField<String>()
    val cbPolicy = ObservableField<Boolean>()

    fun signUp() = bindUiState {
        authRepository.signUp(
            AccountRequest(
                email = edtEmail.get().orEmpty(),
                password = edtPassword.get().orEmpty(),
                agreePolicy = cbPolicy.get() ?: false
            )
        ).onSuccess {
            navigator.navigateTo(
                SignUpFragmentDirections.actionSignUpFragmentToOtpFragment(
                    edtEmail.get().orEmpty()
                )
            )
        }
    }

    fun back() = navigator.back()
}