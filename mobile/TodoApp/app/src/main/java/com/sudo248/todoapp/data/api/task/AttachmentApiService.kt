package com.sudo248.todoapp.data.api.task

import com.sudo248.base_android_annotation.apiservice.ApiService
import com.sudo248.base_android_annotation.apiservice.EnableAuthentication
import com.sudo248.base_android_annotation.apiservice.logging_level.Level
import com.sudo248.base_android_annotation.apiservice.logging_level.LoggingLever
import com.sudo248.todoapp.BuildConfig
import com.sudo248.todoapp.data.SharedPrefKeys
import com.sudo248.todoapp.data.api.BaseResponse
import com.sudo248.todoapp.domain.entity.attachment.AttachFile
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.DELETE
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path

@ApiService(baseUrl = BuildConfig.BASE_URL)
@EnableAuthentication(SharedPrefKeys.TOKEN)
@LoggingLever(level = Level.BODY)
interface AttachmentApiService {

    @Multipart
    @POST("attachments/upload/{taskId}")
    suspend fun uploadAttachment(@Path("taskId") taskId: Int, @Part file: MultipartBody.Part): Response<BaseResponse<AttachFile>>

    @DELETE("attachments/{attachmentId}")
    suspend fun deleteAttachment(@Path("attachmentId") attachmentId: String): Response<BaseResponse<Any>>
}