package com.sudo248.todoapp.ui.activity.main

import androidx.lifecycle.AndroidViewModel
import com.sudo248.base_android.base.BaseViewModel
import com.sudo248.base_android.core.DataState
import com.sudo248.base_android.core.UiState
import com.sudo248.base_android.core.event.SingleEvent
import com.sudo248.base_android.ktx.bindUiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

abstract class BaseAppViewModel<Directions> : BaseViewModel<Directions>() {
    protected fun bindUiState(state: MutableStateFlow<SingleEvent<UiState>> = _uiState, handler: suspend () -> DataState<*, *>) = launch {
        state.emit(SingleEvent(UiState.LOADING))
        val result = try {
            handler.invoke()
        } catch (e: Exception) {
            DataState.Error(e)
        }
        if (result.isError) error = SingleEvent(result.error())
        result.bindUiState(state)
    }
}