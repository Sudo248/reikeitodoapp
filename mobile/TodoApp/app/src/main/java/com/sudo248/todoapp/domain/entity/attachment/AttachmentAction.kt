package com.sudo248.todoapp.domain.entity.attachment

enum class AttachmentAction {
    ACTION_CLICK_ITEM,
    ACTION_DELETE_ITEM
}