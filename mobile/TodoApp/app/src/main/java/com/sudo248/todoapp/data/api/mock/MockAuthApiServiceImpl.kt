package com.sudo248.todoapp.data.api.mock

import com.sudo248.base_android.utils.AppSharedPreference
import com.sudo248.todoapp.data.SharedPrefKeys
import com.sudo248.todoapp.data.api.BaseResponse
import com.sudo248.todoapp.data.api.auth.AuthApiService
import com.sudo248.todoapp.data.api.auth.request.AccountRequest
import com.sudo248.todoapp.data.api.auth.request.OtpRequest
import com.sudo248.todoapp.data.api.auth.response.TokenResponse
import com.sudo248.todoapp.domain.entity.task.Task
import okhttp3.ResponseBody
import retrofit2.Response

class MockAuthApiServiceImpl: AuthApiService {
    override suspend fun login(ar: AccountRequest): Response<BaseResponse<TokenResponse>> {
        return MockData.accounts.keys.firstOrNull() {
            it.email == ar.email && it.password == ar.password
        }?.let {
            val token: String? = MockData.accounts[it]?.let { it1 -> it1.token }
            AppSharedPreference.I.putString(SharedPrefKeys.TOKEN, token)
            Response.success(
                200,
                BaseResponse(
                    success = true,
                    message = "Success",
                    data = token?.let { it1 -> TokenResponse(it1) }
                )
            )
        } ?: Response.error(
            404, ResponseBody.create(
                null, MockData.gson.toJson(
                    BaseResponse(
                        success = false,
                        message = "Not found user",
                        data = null,
                    )
                )
            )
        )
    }

    override suspend fun signUp(accountRequest: AccountRequest): Response<BaseResponse<Any>> {
        TODO("Not yet implemented")
    }

    override suspend fun confirmOtp(otpRequest: OtpRequest): Response<BaseResponse<TokenResponse>> {
        TODO("Not yet implemented")
    }
}