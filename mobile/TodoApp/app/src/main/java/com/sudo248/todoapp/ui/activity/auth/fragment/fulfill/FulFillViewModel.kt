package com.sudo248.todoapp.ui.activity.auth.fragment.fulfill

import android.view.View
import android.widget.AdapterView
import androidx.databinding.ObservableField
import androidx.navigation.NavDirections
import com.sudo248.base_android.ktx.createActionIntentDirections
import com.sudo248.base_android.ktx.onSuccess
import com.sudo248.todoapp.R
import com.sudo248.todoapp.domain.entity.auth.Profile
import com.sudo248.todoapp.domain.repository.AuthRepository
import com.sudo248.todoapp.domain.repository.ProfileRepository
import com.sudo248.todoapp.ui.activity.main.BaseAppViewModel
import com.sudo248.todoapp.ui.activity.main.MainActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import java.time.LocalDate
import java.util.Calendar
import javax.inject.Inject

@HiltViewModel
class FulFillViewModel @Inject constructor(
    private val profileRepository: ProfileRepository
) : BaseAppViewModel<NavDirections>() {

    val edtFirstName = ObservableField<String>()
    val edtLastName = ObservableField<String>()

    val dateData = ObservableField((1..31).toList())
    val monthData = ObservableField((1..12).toList())
    val yearData = ObservableField((1920..Calendar.getInstance().get(Calendar.YEAR)).reversed().toList())

    private var dateOfMoth: Int = 1
    private var month: Int = 1
    private var year: Int = 1

    fun onClickContinue() = bindUiState {
        profileRepository.createProfile(
            Profile(
                firstName = edtFirstName.get().orEmpty(),
                lastName = edtLastName.get().orEmpty(),
                dob = getDob()
            )
        ).onSuccess {
            viewController<FulFillViewController>()
                .navigateToActivity(MainActivity::class.createActionIntentDirections())
        }
    }

    private fun getDob(): LocalDate {
        return when {
            dateData.get() == null -> throw Exception("Invalid Date")
            monthData.get() == null -> throw Exception("Invalid Month")
            yearData.get() == null -> throw Exception("Invalid Year")
            else -> {
                LocalDate.of(year, month, dateOfMoth)
            }
        }
    }

    fun back() = navigator.back()

    fun onItemDateSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        dateOfMoth = parent.getItemAtPosition(position) as Int
    }

    fun onItemMonthSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        month = parent.getItemAtPosition(position) as Int
    }

    fun onItemYearSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        year = parent.getItemAtPosition(position) as Int
    }

}