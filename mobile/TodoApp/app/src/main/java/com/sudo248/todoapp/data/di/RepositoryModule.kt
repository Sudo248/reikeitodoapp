package com.sudo248.todoapp.data.di

import com.sudo248.todoapp.data.repository.AttachmentRepositoryImpl
import com.sudo248.todoapp.data.repository.AuthRepositoryImpl
import com.sudo248.todoapp.data.repository.CategoryRepositoryImpl
import com.sudo248.todoapp.data.repository.ProfileRepositoryImpl
import com.sudo248.todoapp.data.repository.TaskRepositoryImpl
import com.sudo248.todoapp.domain.repository.AttachmentRepository
import com.sudo248.todoapp.domain.repository.AuthRepository
import com.sudo248.todoapp.domain.repository.CategoryRepository
import com.sudo248.todoapp.domain.repository.ProfileRepository
import com.sudo248.todoapp.domain.repository.TaskRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindAuthRepository(authRepositoryImpl: AuthRepositoryImpl): AuthRepository

    @Binds
    abstract fun bindCategoryRepository(categoryRepositoryImpl: CategoryRepositoryImpl): CategoryRepository

    @Binds
    abstract fun bindTaskRepository(taskRepositoryImpl: TaskRepositoryImpl): TaskRepository

    @Binds
    abstract fun bindAttachmentRepository(attachmentRepositoryImpl: AttachmentRepositoryImpl): AttachmentRepository

    @Binds
    abstract fun bindProfileRepository(profileRepositoryImpl: ProfileRepositoryImpl): ProfileRepository

}