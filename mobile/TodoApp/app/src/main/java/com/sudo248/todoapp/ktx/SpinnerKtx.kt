package com.sudo248.todoapp.ktx

import android.annotation.SuppressLint
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import com.sudo248.todoapp.R

@SuppressLint("ResourceType")
@BindingAdapter("setData")
fun setData(spinner: Spinner, data: List<Any>?) {
    if (data == null) return
    spinner.adapter =
        ArrayAdapter(spinner.context, R.layout.item_textview, data)
}