package com.sudo248.todoapp.ui.activity.main.adapter

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.sudo248.base_android.base.BaseListAdapter
import com.sudo248.base_android.base.BaseViewHolder
import com.sudo248.todoapp.R
import com.sudo248.todoapp.databinding.ItemCategoryBinding
import com.sudo248.todoapp.domain.entity.category.Category

class CategoryAdapter(
    private val onItemClick: (Category) -> Unit
) : BaseListAdapter<Category, CategoryAdapter.CategoryViewHolder>() {

    private lateinit var lastSelectedItemBinding: ItemCategoryBinding
    var currentSelectedPosition = 0

    override fun getViewHolder(
        layoutInflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<Category, *> {
        return CategoryViewHolder(ItemCategoryBinding.inflate(layoutInflater, parent, false))
    }

    fun submitData(list: List<Category>?, currentCategoryId: Int?) {
        currentSelectedPosition = currentCategoryId?.let {
            list?.indexOfFirst { it.categoryId == currentCategoryId && it.isShow } ?: 0
        } ?: 0
        super.submitList(list?.filter { it.isShow })
        notifyItemChanged(currentSelectedPosition)
    }

    inner class CategoryViewHolder(binding: ItemCategoryBinding) :
        BaseViewHolder<Category, ItemCategoryBinding>(binding) {
        override fun onBind(item: Category) {
            if (adapterPosition == currentSelectedPosition) {
                lastSelectedItemBinding = binding
                setItem(binding, isSelected = true)
            } else {
                setItem(binding, isSelected = false)
            }

            binding.txtType.text = item.name
            binding.root.setOnClickListener {
                if (!it.isSelected) {
                    selectedCurrentItem()
                    onItemClick.invoke(item)
                }
                currentSelectedPosition = adapterPosition
            }
        }

        private fun selectedCurrentItem() {
            setItem(lastSelectedItemBinding, isSelected = false)
            setItem(binding, isSelected = true)
            lastSelectedItemBinding = binding
        }

        private fun setItem(itemBinding: ItemCategoryBinding, isSelected: Boolean) {
            itemBinding.apply {
                root.isSelected = isSelected
                txtType.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        itemView.context,
                        if (isSelected) R.color.primaryColor else R.color.primaryLightColor
                    )
                )
                txtType.setTextColor(
                    ContextCompat.getColor(
                        itemView.context,
                        if (isSelected) R.color.white else R.color.gray_75
                    )
                )
            }
        }
    }

}