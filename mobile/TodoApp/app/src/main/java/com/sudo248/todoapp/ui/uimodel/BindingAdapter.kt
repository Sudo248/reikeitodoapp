package com.sudo248.todoapp.ui.uimodel

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.sudo248.todoapp.BuildConfig
import com.sudo248.todoapp.R

@BindingAdapter("imageUrl")
fun loadImage(image: ImageView, url: String) {
    if (url.isEmpty()) return
    var imageUrl = url
    if (!imageUrl.startsWith("http")) imageUrl = "${BuildConfig.BASE_URL}images/$url"
    val circularProgressDrawable = CircularProgressDrawable(image.context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()
    Glide
        .with(image.context)
        .load(imageUrl)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .placeholder(circularProgressDrawable)
        .error(R.drawable.ic_error)
        .into(image)
}