package com.sudo248.todoapp.data.repository

import com.sudo248.base_android.core.DataState
import com.sudo248.base_android.data.api.handleResponse
import com.sudo248.base_android.ktx.stateOn
import com.sudo248.todoapp.data.api.category.CategoryApiService
import com.sudo248.todoapp.data.utils.boundTime
import com.sudo248.todoapp.domain.Constants
import com.sudo248.todoapp.domain.entity.category.Category
import com.sudo248.todoapp.domain.repository.CategoryRepository
import com.sudo248.todoapp.ktx.data
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CategoryRepositoryImpl @Inject constructor(
    private val categoryApiService: CategoryApiService,
    private val ioDispatcher: CoroutineDispatcher
) : CategoryRepository {

    override suspend fun getAllCategories(needCategoryAll: Boolean): DataState<List<Category>, Exception> =
        stateOn(ioDispatcher) {
            val response =
                handleResponse(boundTime { categoryApiService.getCategories(needCategoryAll) })
            response.data()
        }

    override suspend fun createNewCategory(category: Category): DataState<Category, Exception> =
        stateOn(ioDispatcher) {
            val response =
                handleResponse(boundTime { categoryApiService.postCategory(category) })
            response.data()
        }

    override suspend fun updateCategory(category: Category): DataState<Category, Exception> =
        stateOn(ioDispatcher) {
            val response =
                handleResponse(boundTime {
                    categoryApiService.putCategory(
                        category.categoryId!!,
                        category
                    )
                })
            response.data()
        }

    override suspend fun deleteCategory(categoryId: Int): DataState<Unit, Exception> =
        stateOn(ioDispatcher) {
            val response =
                handleResponse(boundTime { categoryApiService.deleteCategory(categoryId) })
            response.data()
        }
}