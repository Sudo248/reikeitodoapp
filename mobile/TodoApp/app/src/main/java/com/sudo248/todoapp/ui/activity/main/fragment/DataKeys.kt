package com.sudo248.todoapp.ui.activity.main.fragment

object DataKeys {
    const val IS_UPDATE = "IS_UPDATE"
    const val IS_DELETE = "IS_DELETE"
    const val NEED_RELOAD = "NEED_RELOAD"
    const val TASK = "TASK"
    const val CATEGORY = "CATEGORY"

}