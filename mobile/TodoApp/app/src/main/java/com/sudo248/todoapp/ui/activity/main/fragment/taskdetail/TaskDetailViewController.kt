package com.sudo248.todoapp.ui.activity.main.fragment.taskdetail

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Intent
import androidx.annotation.StringRes
import com.sudo248.base_android.base.BaseViewController
import java.time.LocalDate
import java.time.LocalTime

interface TaskDetailViewController : BaseViewController {
    fun getString(@StringRes id: Int): String
    fun getString(@StringRes id: Int, vararg format: Any): String
    fun showDatePicker(selectedDate: LocalDate? = null, onDateSelected: (LocalDate) -> Unit)
    fun showTimePicker(selectedTime: LocalTime? = null, onTimeSelected: (LocalTime) -> Unit)
    fun pickAttachment()
    fun getIntent(clazz: Class<*>): Intent
    fun getAlarmManager(): AlarmManager
    fun getBroadcast(requestCode: Int, intent: Intent, flags: Int): PendingIntent
}