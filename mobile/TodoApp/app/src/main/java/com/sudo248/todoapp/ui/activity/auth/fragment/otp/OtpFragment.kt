package com.sudo248.todoapp.ui.activity.auth.fragment.otp

import androidx.core.content.ContextCompat
import androidx.databinding.ObservableField
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.sudo248.base_android.base.BaseFragment
import com.sudo248.base_android.utils.AppToast
import com.sudo248.todoapp.R
import com.sudo248.todoapp.databinding.FragmentOtpBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OtpFragment : BaseFragment<FragmentOtpBinding, OtpViewModel>() {
    override val viewModel: OtpViewModel by viewModels()
    private val args: OtpFragmentArgs by navArgs()
    override val enableStateScreen: Boolean
        get() = true

    override fun initView() {
        viewModel.email = args.email
        viewModel.startTimer()
        binding.edtOtp.setOnFulFillListener {
            viewModel.onFullFillListener(it)
        }
    }

    override fun observer() {
        viewModel.timeout.observe(viewLifecycleOwner) {
            binding.txtDescription.text = getString(R.string.otp_description, args.email, it)
        }
    }

    override fun onStateError() {
        super.onStateError()
        viewModel.errorMessage?.let { AppToast.show(it) }
    }
}