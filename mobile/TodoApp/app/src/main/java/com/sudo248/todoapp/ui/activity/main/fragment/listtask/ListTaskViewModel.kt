package com.sudo248.todoapp.ui.activity.main.fragment.listtask

import android.app.Dialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.sudo248.base_android.core.UiState
import com.sudo248.base_android.core.bus.Subscriber
import com.sudo248.base_android.core.bus.SudoBus
import com.sudo248.base_android.core.event.SingleEvent
import com.sudo248.base_android.ktx.onSuccess
import com.sudo248.base_android.ktx.setTransparentBackground
import com.sudo248.base_android.ktx.showWithTransparentBackground
import com.sudo248.base_android.navigation.ResultCallback
import com.sudo248.todoapp.R
import com.sudo248.todoapp.databinding.BottomsheetCreateTaskBinding
import com.sudo248.todoapp.databinding.DialogSortTaskBinding
import com.sudo248.todoapp.domain.entity.category.Category
import com.sudo248.todoapp.domain.entity.task.Task
import com.sudo248.todoapp.domain.entity.task.TaskAction
import com.sudo248.todoapp.domain.entity.task.TaskSortCondition
import com.sudo248.todoapp.domain.repository.CategoryRepository
import com.sudo248.todoapp.domain.repository.TaskRepository
import com.sudo248.todoapp.ktx.getSerializableSafety
import com.sudo248.todoapp.ui.activity.main.BaseAppViewModel
import com.sudo248.todoapp.ui.activity.main.builder.PopupMenuBuilder
import com.sudo248.todoapp.ui.activity.main.fragment.DataKeys
import com.sudo248.todoapp.ui.activity.main.fragment.EventKeys
import com.sudo248.todoapp.ui.activity.main.fragment.NavigationKeys
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.time.LocalDate
import javax.inject.Inject

@HiltViewModel
class ListTaskViewModel @Inject constructor(
    private val categoryRepository: CategoryRepository,
    private val taskRepository: TaskRepository
) : BaseAppViewModel<NavDirections>() {

    private val _categories = MutableLiveData<List<Category>>()
    val categories: LiveData<List<Category>> = _categories

    private val _tasks = MutableLiveData<List<Task>>()
    val tasks: LiveData<List<Task>> = _tasks

    var currentCategoryId: Int? = -1

    val isSearchMode = ObservableField(false)

    private var currentQuerySearch = ""

    private var currentTaskSortCondition: TaskSortCondition

    private val _uiStateTasks = MutableStateFlow(SingleEvent(UiState.IDLE))
    val uiStateTasks: StateFlow<SingleEvent<UiState>> = _uiStateTasks

    private var jobSearchTask: Job? = null

    private val controller: ListTaskViewController
        get() = viewController()

    init {
        subscribeBus()
        currentTaskSortCondition = taskRepository.getTaskSortCondition()
        getAllCategory()
    }

    fun onRefreshTask() {
        onClickItemCategory(getCurrentCategory() , true)
    }

    private fun getAllCategory(refresh: Boolean = false) = bindUiState {
        categoryRepository.getAllCategories(needCategoryAll = true)
            .onSuccess {
                _categories.postValue(it)
                onClickItemCategory(getCurrentCategory(it) ?: it.first(), refresh)
            }
    }

    fun onOpenMenu(view: View) {
        PopupMenuBuilder(view)
            .setMenu(R.menu.popup_menu_list_task)
            .setOnItemMenuClickListener {
                onItemMenuSelected(view, it)
            }
            .build()
            .show()
    }

    private fun onItemMenuSelected(view: View, menuItem: MenuItem) {
        when (menuItem.itemId) {
            R.id.manageType -> {
                navigator.navigateForResult(
                    ListTaskFragmentDirections.actionCategoryFragmentToListCategoryFragment(),
                    NavigationKeys.ACTION_TO_CATEGORY_FRAGMENT,
                    object : ResultCallback {
                        override fun onResult(key: String, data: Bundle?) {
                            if (key == NavigationKeys.ACTION_TO_CATEGORY_FRAGMENT && data != null) {
                                handleResultFromCategory(data)
                            }
                        }
                    }
                )
            }

            R.id.search -> {
                isSearchMode.set(true)
                search("")
            }

            R.id.sortTask -> {
                showDialogSortBy(view)
            }
        }
    }

    fun onClickItemCategory(category: Category?, refresh: Boolean = false) {
        currentCategoryId = category?.categoryId ?: -1
        if (refresh || category?.tasks.isNullOrEmpty()) {
            bindUiState(_uiStateTasks) {
                taskRepository.getTasks(categoryId = category?.categoryId!!)
                    .onSuccess {
                        category.tasks = it
                        postTasksOfCategory(category)
                    }
            }
        } else {
            postTasksOfCategory(category)
        }
    }

    fun onClickItemTask(task: Task, action: TaskAction) = launch {
        when (action) {
            TaskAction.ACTION_CLICK_ITEM -> {
                navigator.navigateForResult(
                    ListTaskFragmentDirections.actionCategoryFragmentToTaskDetailFragment(task),
                    NavigationKeys.ACTION_TO_DETAIL_FRAGMENT,
                    object : ResultCallback {
                        override fun onResult(key: String, data: Bundle?) {
                            if (key == NavigationKeys.ACTION_TO_DETAIL_FRAGMENT && data != null) {
                                handleResultFromTaskDetail(task, data)
                            }
                        }
                    }
                )
            }

            TaskAction.ACTION_CHANGE_FLAG, TaskAction.ACTION_CHANGE_STATUS -> {
                bindUiState(_uiStateTasks) {
                    taskRepository.updateTasks(task)
                        .onSuccess {
                            upsertTask(it)
                        }
                }
            }
        }
    }

    private fun upsertTask(task: Task) {
        _categories.value?.let { it ->
            val categories = it.toMutableList()
            categories[0] = upsertTaskOfCategory(categories[0], task)
            if (task.categoryId != null && task.categoryId != -1) {
                categories.indexOfFirst { it.categoryId == task.categoryId }
                    .takeIf { it != -1 }?.let { indexCategory ->
                        categories[indexCategory] =
                            upsertTaskOfCategory(categories[indexCategory], task)
                    }
            }
            _categories.postValue(categories)
            if (isSearchMode.get() == true) {
                search(currentQuerySearch)
            } else {
                postTasksOfCategory(getCurrentCategory())
            }
        }
    }

    private fun upsertTaskOfCategory(
        category: Category,
        task: Task
    ): Category {
        val currentCategoryTask = category.tasks.orEmpty().toMutableList()
        val indexTask = currentCategoryTask.indexOfFirst { it.taskId == task.taskId }
        if (indexTask == -1) {
            currentCategoryTask.add(task)
        } else {
            currentCategoryTask[indexTask] = task
        }
        category.tasks = currentCategoryTask
        return category
    }

    private fun deleteTask(task: Task, deleteAll: Boolean = true) {
        _categories.value?.let { it ->
            val categories = it.toMutableList()
            if (deleteAll) categories[0] = deleteTaskOfCategory(categories[0], task)
            if (task.categoryId != null && task.categoryId != -1) {
                categories.indexOfFirst { it.categoryId == task.categoryId }
                    .takeIf { it != -1 }?.let { indexCategory ->
                        categories[indexCategory] =
                            deleteTaskOfCategory(categories[indexCategory], task)
                    }
            }
            _categories.postValue(categories)
            postTasksOfCategory(getCurrentCategory())
        }
    }

    private fun deleteTaskOfCategory(category: Category, task: Task): Category {
        val currentCategoryTask = category.tasks.orEmpty().toMutableList()
        currentCategoryTask.removeIf { it.taskId == task.taskId }
        category.tasks = currentCategoryTask
        return category
    }

    private fun handleResultFromTaskDetail(oldTask: Task, data: Bundle) {
        when {
            data.getBoolean(DataKeys.IS_UPDATE) -> {
                val updatedTask = data.getSerializableSafety<Task>(DataKeys.TASK)
                updatedTask?.let {
                    if (oldTask.categoryId != it.categoryId) {
                        deleteTask(oldTask, deleteAll = false)
                    }
                    upsertTask(updatedTask)
                }
            }

            data.getBoolean(DataKeys.IS_DELETE) -> {
                deleteTask(oldTask)
            }
        }
    }

    private fun handleResultFromCategory(data: Bundle) {
        if (data.getBoolean(DataKeys.NEED_RELOAD, true)) {
            getAllCategory(refresh = true)
        }
    }

    private fun getCurrentCategory(categories: List<Category>? = null): Category? {
        return if (currentCategoryId == null || currentCategoryId == -1) _categories.value?.get(0)
        else (categories
            ?: _categories.value)?.firstOrNull { it.categoryId == currentCategoryId && it.isShow }
    }

    fun exitSearchMode() {
        isSearchMode.set(false)
        currentQuerySearch = ""
        postTasksOfCategory(getCurrentCategory())
    }

    fun onSearch(newQuery: String) {
        if (isSearchMode.get() == false) return
        jobSearchTask?.cancel()
        jobSearchTask = launch {
            delay(500)
            search(newQuery)
        }
    }

    fun search(query: String) {
        if (isSearchMode.get() == false) return
        currentQuerySearch = query
        val searchList = _categories.value?.get(0)?.tasks?.filter {
            it.title.lowercase().contains(query.lowercase())
        }.orEmpty()
        _tasks.postValue(searchList.sortedByCondition(currentTaskSortCondition))
    }

    private fun showDialogSortBy(view: View) {
        val dialog = Dialog(view.context)
        val dialogBinding = DialogSortTaskBinding.inflate(
            LayoutInflater.from(view.context),
            view.parent as? ViewGroup,
            false
        )

        dialog.setContentView(dialogBinding.root)

        dialogBinding.apply {
            when (currentTaskSortCondition) {
                TaskSortCondition.SORT_BY_DEADLINE -> rbSortByDeadline.isChecked = true
                TaskSortCondition.SORT_BY_PRIORITY -> rbSortByPriority.isChecked = true
                TaskSortCondition.SORT_BY_STATUS -> rbSortByStatus.isChecked = true
                TaskSortCondition.SORT_BY_CREATE_TIME -> rbSortByCreateTime.isChecked = true
                TaskSortCondition.SORT_BY_ALPHABET -> rbSortByAlphabet.isChecked = true
            }

            txtApplySort.setOnClickListener {
                dialog.hide()
                val condition = when (group.checkedRadioButtonId) {
                    R.id.rbSortByPriority -> TaskSortCondition.SORT_BY_PRIORITY
                    R.id.rbSortByStatus -> TaskSortCondition.SORT_BY_STATUS
                    R.id.rbSortByCreateTime -> TaskSortCondition.SORT_BY_CREATE_TIME
                    R.id.rbSortByAlphabet -> TaskSortCondition.SORT_BY_ALPHABET
                    else -> {
                        TaskSortCondition.SORT_BY_DEADLINE
                    }
                }
                applyTaskSortCondition(condition)
            }
        }

        dialog.showWithTransparentBackground()
    }

    private fun applyTaskSortCondition(condition: TaskSortCondition) {
        if (condition != currentTaskSortCondition) {
            taskRepository.setTaskSortCondition(condition)
            currentTaskSortCondition = condition
            if (isSearchMode.get() == true) {
                _tasks.postValue(_tasks.value.sortedByCondition(currentTaskSortCondition))
            } else {
                postTasksOfCategory(getCurrentCategory())
            }
        }
    }

    private fun List<Task>?.sortedByCondition(condition: TaskSortCondition): List<Task> {
        if (isNullOrEmpty()) return listOf()
        return when (condition) {
            TaskSortCondition.SORT_BY_DEADLINE -> sortedWith(
                compareBy(
                    { it.deadline },
                    { it.timeAlert }
                )
            )

            TaskSortCondition.SORT_BY_PRIORITY -> sortedByDescending { it.color }

            TaskSortCondition.SORT_BY_STATUS -> sortedBy { it.status }

            TaskSortCondition.SORT_BY_CREATE_TIME -> sortedBy { it.createdDate }

            TaskSortCondition.SORT_BY_ALPHABET -> sortedBy { it.title }
        }
    }

    private fun postTasksOfCategory(category: Category?) {
        if (category == null || category.tasks.isNullOrEmpty()) return _tasks.postValue(listOf())
        val tasks = category.tasks!!
//        if (category.taskSortCondition == currentTaskSortCondition) return _tasks.postValue(tasks)
        category.taskSortCondition = currentTaskSortCondition
        val sortedList = tasks.sortedByCondition(currentTaskSortCondition)
        if (category.categoryId == null || category.categoryId == -1) {
            category.tasks = filterTaskOfShowCategory(sortedList)
        } else {
            category.tasks = sortedList
        }
        _tasks.postValue(category.tasks.orEmpty())
    }

    fun onClickAddTask(view: View) {
        val bottomSheet = BottomSheetDialog(view.context, R.style.AppBottomSheetDialog)
        val bottomSheetBinding = BottomsheetCreateTaskBinding.inflate(
            LayoutInflater.from(view.context),
            view.parent as? ViewGroup,
            false
        )
        bottomSheet.setContentView(bottomSheetBinding.root)

        var deadline: LocalDate? = null

        bottomSheetBinding.apply {
            val categories = _categories.value.orEmpty()
            val categoryNames = categories.map {
                if (it.categoryId == null || it.categoryId == -1) {
                    controller.getString(R.string.no_type)
                } else {
                    it.name
                }
            }
            val categoryAdapter = ArrayAdapter(view.context, R.layout.item_textview, categoryNames)
            (bottomSheetBinding.tilCategory.editText as? AutoCompleteTextView)?.apply {
                setText(categoryNames[0])
                setAdapter(categoryAdapter)
            }

            imgCalender.setOnClickListener {
                controller.showDatePicker { date ->
                    deadline = date
                }
            }

            imgSubmit.isEnabled = false

            edtTaskTitle.doOnTextChanged { _, _, _, count ->
                if (count > 0 && !imgSubmit.isEnabled) {
                    imgSubmit.isEnabled = true
                    imgSubmit.backgroundTintList = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            view.context,
                            R.color.primaryColor
                        )
                    )
                }

                if (count <= 0 && imgSubmit.isEnabled) {
                    imgSubmit.isEnabled = false
                    imgSubmit.backgroundTintList = ColorStateList.valueOf(
                        ContextCompat.getColor(
                            view.context,
                            R.color.gray_88
                        )
                    )
                }
            }

            imgSubmit.setOnClickListener {
                bottomSheet.hide()
                val newTask = Task(
                    categoryId = getCategoryIdByName(tilCategory.editText?.text.toString()),
                    title = bottomSheetBinding.edtTaskTitle.text.toString(),
                    createdDate = LocalDate.now(),
                    updatedDate = LocalDate.now(),
                    deadline = deadline ?: LocalDate.now()
                )

                bindUiState {
                    taskRepository.createNewTask(newTask)
                        .onSuccess {
                            upsertTask(it)
                        }
                }
            }
        }
        bottomSheet.setOnShowListener {
            bottomSheetBinding.edtTaskTitle.requestFocus()
        }
        bottomSheet.setTransparentBackground()
        bottomSheet.show()
    }

    private fun getCategoryIdByName(name: String?): Int? {
        if (name == null || _categories.value.isNullOrEmpty()) return null
        return _categories.value?.firstOrNull { it.name == name }?.categoryId
    }

    private fun subscribeBus() {
        SudoBus.getDefaultInstance()
            .subscribe(EventKeys.EVENT_UPDATE_TASK, object : Subscriber<Task>() {
                override suspend fun onReceive(event: Task) {
                    upsertTask(task = event)
                }
            })

        SudoBus.getDefaultInstance()
            .subscribe(EventKeys.EVENT_DELETE_TASK, object : Subscriber<Task>() {
                override suspend fun onReceive(event: Task) {
                    deleteTask(task = event)
                }
            })
    }

    private fun filterTaskOfShowCategory(tasks: List<Task>): List<Task> {
        val showCategories = _categories.value.orEmpty().filter { it.isShow }.map { it.categoryId }
        return tasks.filter {
            it.categoryId == null || showCategories.contains(it.categoryId)
        }
    }
}