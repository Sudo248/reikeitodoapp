package com.sudo248.todoapp.ui.activity.main.fragment.listtask

import androidx.annotation.StringRes
import com.sudo248.base_android.base.BaseViewController
import java.time.LocalDate

interface ListTaskViewController : BaseViewController {
    fun getString(@StringRes id: Int): String
    fun showDatePicker(selectedDate: LocalDate? = null, onDateSelected: (LocalDate) -> Unit)
}