package com.sudo248.todoapp.ui.activity.auth.fragment.fulfill

import androidx.fragment.app.viewModels
import com.sudo248.base_android.base.BaseFragment
import com.sudo248.base_android.navigation.IntentDirections
import com.sudo248.todoapp.databinding.FragmentFulfillBinding
import com.sudo248.todoapp.ui.activity.auth.AuthActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FulFillFragment : BaseFragment<FragmentFulfillBinding, FulFillViewModel>(), FulFillViewController {
    override val viewModel: FulFillViewModel by viewModels()
    override val enableStateScreen: Boolean
        get() = true

    override fun initView() {
        binding.viewModel = viewModel
    }

    override fun navigateToActivity(directions: IntentDirections) {
        (activity as AuthActivity).navigateTo(directions)
    }


}