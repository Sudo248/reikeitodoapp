package com.sudo248.todoapp.data.api.auth

import com.sudo248.base_android_annotation.apiservice.ApiService
import com.sudo248.base_android_annotation.apiservice.logging_level.Level
import com.sudo248.base_android_annotation.apiservice.logging_level.LoggingLever
import com.sudo248.todoapp.BuildConfig
import com.sudo248.todoapp.data.api.BaseResponse
import com.sudo248.todoapp.data.api.auth.request.AccountRequest
import com.sudo248.todoapp.data.api.auth.request.OtpRequest
import com.sudo248.todoapp.data.api.auth.response.TokenResponse
import com.sudo248.todoapp.domain.entity.auth.Profile
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST


@ApiService(baseUrl = BuildConfig.BASE_URL)
@LoggingLever(level = Level.BODY)
interface AuthApiService {

    @POST("auth/login")
    suspend fun login(@Body accountRequest: AccountRequest): Response<BaseResponse<TokenResponse>>

    @POST("auth/signup")
    suspend fun signUp(@Body accountRequest: AccountRequest): Response<BaseResponse<Any>>

    @POST("auth/otp-verify")
    suspend fun confirmOtp(@Body otpRequest: OtpRequest): Response<BaseResponse<TokenResponse>>


}