package com.sudo248.todoapp.ui.activity.auth

import com.sudo248.base_android.base.BaseActivity
import com.sudo248.base_android.base.EmptyViewModel
import com.sudo248.base_android.navigation.IntentDirections
import com.sudo248.todoapp.databinding.ActivityAuthBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AuthActivity : BaseActivity<ActivityAuthBinding, EmptyViewModel<IntentDirections>>() {
    override val viewModel: EmptyViewModel<IntentDirections>
        get() = EmptyViewModel()

    override fun initView() {

    }
}