package com.sudo248.todoapp.ui.widget

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ColorPicker
@JvmOverloads
constructor(
    context: Context,
    attrs: AttributeSet
) : RecyclerView(context, attrs) {

    interface OnPickColorListener {
        fun onPickColor(color: Int, hexColor: String?)
    }

    private var onPickColorListener: OnPickColorListener? = null
    private var initColor: Any? = null

    private val colorPickerAdapter by lazy {
        ColorPickerAdapter(initColor) { color, hexColor ->
            onPickColorListener?.onPickColor(color, hexColor)
        }
    }

    init {
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        adapter = colorPickerAdapter
    }

    fun setOnPickColorListener(onPickColorListener: OnPickColorListener) {
        this.onPickColorListener = onPickColorListener
    }

    fun setColors(colors: List<Any>) {
        colorPickerAdapter.setColors(colors)
    }

    fun setInitColor(color: Any?) {
        this.initColor = color
    }

    fun setItemMargin(margin: Int) {
        colorPickerAdapter.setItemMargin(margin)
    }

    private class ColorPickerAdapter(
        private val initColor: Any?,
        private val onPickColor: (Int, String?) -> Unit
    ) :
        Adapter<ColorPickerAdapter.ColorPickerViewHolder>() {

        private val colors: MutableList<Any> = mutableListOf()
        private var previousSelectedView: CircleView? = null
        private var itemMargin: Int? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorPickerViewHolder {
            return ColorPickerViewHolder(CircleView(parent.context))
        }

        override fun onBindViewHolder(holder: ColorPickerViewHolder, position: Int) {
            val item = colors[position]
            if (item is Int) {
                holder.onBind(item, null, onPickColor)
            } else {
                val hexColor = item as String
                holder.onBind(Color.parseColor(hexColor), hexColor, onPickColor)
            }

            if (item == initColor) {
                holder.view.isSelected = true
                previousSelectedView = holder.view
            }
        }

        override fun getItemCount(): Int = colors.size

        fun setColors(colors: List<Any>) {
            this.colors.clear()
            this.colors.addAll(colors)
            notifyDataSetChanged()
        }

        fun setItemMargin(margin: Int) {
            this.itemMargin = margin
        }

        inner class ColorPickerViewHolder(val view: CircleView) : ViewHolder(view) {
            fun onBind(
                @ColorInt colorInt: Int,
                hexColor: String? = null,
                onPickColor: (Int, String?) -> Unit
            ) {
                view.setColor(colorInt)
                view.setOnClickListener {
                    view.isSelected = true
                    previousSelectedView?.isSelected = false
                    previousSelectedView = view
                    onPickColor(colorInt, hexColor)
                }
            }
        }
    }
}