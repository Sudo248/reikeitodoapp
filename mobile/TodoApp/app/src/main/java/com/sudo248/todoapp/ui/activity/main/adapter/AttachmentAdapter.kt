package com.sudo248.todoapp.ui.activity.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sudo248.base_android.base.BaseListAdapter
import com.sudo248.base_android.base.BaseViewHolder
import com.sudo248.todoapp.databinding.ItemAttachmentBinding
import com.sudo248.todoapp.domain.entity.attachment.AttachFile
import com.sudo248.todoapp.domain.entity.attachment.AttachmentAction
import com.sudo248.todoapp.ui.uimodel.loadImage

class AttachmentAdapter(
    private val onClickItem: (View, AttachFile, AttachmentAction) -> Unit
) : BaseListAdapter<AttachFile, AttachmentAdapter.AttachmentViewHolder>() {

    override fun getViewHolder(
        layoutInflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<AttachFile, *> {
        return AttachmentViewHolder(ItemAttachmentBinding.inflate(layoutInflater, parent, false))
    }

    inner class AttachmentViewHolder(binding: ItemAttachmentBinding) :
        BaseViewHolder<AttachFile, ItemAttachmentBinding>(binding) {
        override fun onBind(item: AttachFile) {
            loadImage(binding.imgPreview, item.url)
            binding.txtName.text = item.name
            binding.imgDelete.setOnClickListener {
                onClickItem(itemView, item, AttachmentAction.ACTION_DELETE_ITEM)
            }

            binding.root.setOnClickListener {
                onClickItem(itemView, item, AttachmentAction.ACTION_CLICK_ITEM)
            }
        }
    }

}