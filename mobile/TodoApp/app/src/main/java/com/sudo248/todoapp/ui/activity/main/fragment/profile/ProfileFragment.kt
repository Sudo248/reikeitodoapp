package com.sudo248.todoapp.ui.activity.main.fragment.profile

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CalendarView
import androidx.fragment.app.viewModels
import com.sudo248.base_android.base.BaseFragment
import com.sudo248.base_android.ktx.onSuccess
import com.sudo248.todoapp.R
import com.sudo248.todoapp.databinding.FragmentProfileBinding
import com.sudo248.todoapp.ui.activity.main.adapter.TaskAdapter
import com.sudo248.todoapp.ui.activity.main.fragment.calendar.CalendarViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDate

@AndroidEntryPoint
class ProfileFragment : BaseFragment<FragmentProfileBinding, ProfileViewModel>() {

    override val viewModel: ProfileViewModel by viewModels()

    override val enableStateScreen: Boolean
        get() = true

    override fun initView() {
        binding.viewModel = viewModel

        viewModel.setSpinner()
        viewModel.getProfile1()
        viewModel.countTask()
    }

    override fun observer() {
        viewModel.profile.observe(viewLifecycleOwner) {
            binding.tvName.text = "${it.firstName} ${it.lastName}"
            viewModel.edtFirstName.set(it.firstName)
            viewModel.edtLastName.set(it.lastName)
            viewModel.datePos.set(viewModel.spinnerDate.indexOf(it.dob.dayOfMonth))
            viewModel.monthPos.set(viewModel.spinnerMonth.indexOf(it.dob.monthValue))
            viewModel.yearPos.set(viewModel.spinnerYear.indexOf(it.dob.year))
        }

        viewModel.countTaskDone.observe(viewLifecycleOwner) {
            binding.include1.tv1.text = it.toString()
        }

        viewModel.countTaskProcess.observe(viewLifecycleOwner) {
            binding.include2.tv1.text = it.toString()
        }
    }

}