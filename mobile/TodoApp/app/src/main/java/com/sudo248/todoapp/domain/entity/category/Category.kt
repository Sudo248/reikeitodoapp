package com.sudo248.todoapp.domain.entity.category

import com.google.gson.annotations.SerializedName
import com.sudo248.base_android.base.ItemDiff
import com.sudo248.todoapp.domain.entity.task.Task
import com.sudo248.todoapp.domain.entity.task.TaskSortCondition
import java.io.Serializable

data class Category(
    @SerializedName("id")
    val categoryId: Int? = null,
    @SerializedName("title")
    val name: String,
    val color: String? = null,
    val isShow: Boolean = true,
    val totalTask: Int = 0,
    var tasks: List<Task>? = null,
    var taskSortCondition: TaskSortCondition? = null
): ItemDiff, Serializable {
    override fun isContentTheSame(other: ItemDiff): Boolean {
        return other is Category &&
                this == other
    }

    override fun isItemTheSame(other: ItemDiff): Boolean {
        return other is Category &&
                this.categoryId == other.categoryId &&
                this.name == other.name
    }

    val countTasks: Int
        get() = if (totalTask > 0) totalTask else tasks.orEmpty().size
}
