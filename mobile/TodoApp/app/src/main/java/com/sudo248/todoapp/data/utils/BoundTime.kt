package com.sudo248.todoapp.data.utils

import com.sudo248.todoapp.domain.Constants
import kotlinx.coroutines.delay

suspend fun <T> boundTime(boundTime: Int = Constants.DEFAULT_BOUND_TIME, block: suspend () -> T): T {
    val start = System.currentTimeMillis()
    val result = block()
    val executeTime = System.currentTimeMillis() - start
    if (executeTime < boundTime) {
        delay(boundTime - executeTime)
    }
    return result
}