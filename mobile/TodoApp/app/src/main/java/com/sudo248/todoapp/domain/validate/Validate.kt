package com.sudo248.todoapp.domain.validate

interface Validate<in T> {
    fun validate(value: T): Boolean
}