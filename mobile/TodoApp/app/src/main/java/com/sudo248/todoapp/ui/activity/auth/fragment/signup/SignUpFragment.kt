package com.sudo248.todoapp.ui.activity.auth.fragment.signup

import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.sudo248.base_android.base.BaseFragment
import com.sudo248.base_android.utils.AppToast
import com.sudo248.base_android.utils.DialogUtils
import com.sudo248.todoapp.R
import com.sudo248.todoapp.databinding.FragmentSignupBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpFragment : BaseFragment<FragmentSignupBinding, SignUpViewModel>() {
    override val viewModel: SignUpViewModel by viewModels()
    override val enableStateScreen: Boolean
        get() = true

    override fun initView() {
        binding.viewModel = viewModel
    }

    override fun onStateError() {
        super.onStateError()
        viewModel.errorMessage?.let { AppToast.show(it) }
    }
}