package com.sudo248.todoapp.data.repository

import com.sudo248.base_android.core.DataState
import com.sudo248.base_android.data.api.handleResponse
import com.sudo248.base_android.ktx.stateOn
import com.sudo248.base_android.utils.AppSharedPreference
import com.sudo248.todoapp.data.SharedPrefKeys
import com.sudo248.todoapp.data.api.auth.AuthApiService
import com.sudo248.todoapp.data.api.auth.request.AccountRequest
import com.sudo248.todoapp.data.api.auth.request.OtpRequest
import com.sudo248.todoapp.data.api.profile.ProfileApiService
import com.sudo248.todoapp.data.utils.boundTime
import com.sudo248.todoapp.domain.entity.auth.Profile
import com.sudo248.todoapp.domain.repository.AuthRepository
import com.sudo248.todoapp.domain.validate.EmailValidate
import com.sudo248.todoapp.domain.validate.Validate
import com.sudo248.todoapp.ktx.data
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthRepositoryImpl @Inject constructor(
    private val authApiService: AuthApiService,
    private val ioDispatcher: CoroutineDispatcher
) : AuthRepository {

    private val emailValidate: Validate<String> = EmailValidate()

    override suspend fun login(accountRequest: AccountRequest): DataState<Unit, Exception> =
        stateOn(ioDispatcher) {
            if (!emailValidate.validate(accountRequest.email)) throw Exception("Invalid Email")
            val response = handleResponse(authApiService.login(accountRequest))
            if (response.isSuccess) {
                saveToken(response.data().token)
            }
            response.data()
        }

    override suspend fun signUp(accountRequest: AccountRequest): DataState<Unit, Exception> =
        stateOn(ioDispatcher) {
            if (!emailValidate.validate(accountRequest.email)) throw Exception("Invalid Email")
            val response = handleResponse(authApiService.signUp(accountRequest))
            response.data()
        }

    override suspend fun confirmOtp(otpRequest: OtpRequest): DataState<Unit, Exception> =
        stateOn(ioDispatcher) {
            val response = handleResponse(boundTime { authApiService.confirmOtp(otpRequest) })
            if (response.isSuccess) {
                saveToken(response.data().token)
            }
            response.data()
        }

    override suspend fun logout(): DataState<Unit, Exception> = stateOn(ioDispatcher) {
        clearToken()
        DataState.Success(Unit)
    }

    private fun saveToken(token: String) {
        AppSharedPreference.I.putString(SharedPrefKeys.TOKEN, token)
    }

    private fun clearToken() {
        AppSharedPreference.I.putString(SharedPrefKeys.TOKEN, null)
    }
}