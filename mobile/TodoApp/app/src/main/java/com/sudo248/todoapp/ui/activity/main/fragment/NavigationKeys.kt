package com.sudo248.todoapp.ui.activity.main.fragment

object NavigationKeys {
    const val ACTION_TO_DETAIL_FRAGMENT = "ACTION_TO_DETAIL_FRAGMENT"
    const val ACTION_TO_CATEGORY_FRAGMENT = "ACTION_TO_CATEGORY_FRAGMENT"
}