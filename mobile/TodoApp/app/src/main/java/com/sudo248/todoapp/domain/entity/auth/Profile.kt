package com.sudo248.todoapp.domain.entity.auth

import com.google.gson.annotations.SerializedName
import java.time.LocalDate

data class Profile(
    @SerializedName("id")
    var profileId: Int? = null,
    var firstName: String,
    var lastName: String,
    var dob: LocalDate,
)
