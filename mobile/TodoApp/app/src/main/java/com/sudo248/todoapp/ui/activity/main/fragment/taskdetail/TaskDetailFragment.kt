package com.sudo248.todoapp.ui.activity.main.fragment.taskdetail

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat.CLOCK_24H
import com.sudo248.base_android.base.BaseFragment
import com.sudo248.base_android.utils.AppToast
import com.sudo248.base_android.utils.DialogUtils
import com.sudo248.todoapp.R
import com.sudo248.todoapp.databinding.FragmentTaskDetailBinding
import com.sudo248.todoapp.ui.activity.main.adapter.AttachmentAdapter
import com.sudo248.todoapp.ui.utils.FileUtils
import dagger.hilt.android.AndroidEntryPoint
import java.time.Instant
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId

@AndroidEntryPoint
class TaskDetailFragment : BaseFragment<FragmentTaskDetailBinding, TaskDetailViewModel>(),
    TaskDetailViewController {
    override val viewModel: TaskDetailViewModel by viewModels()
    override val enableStateScreen: Boolean
        get() = true

    private val args: TaskDetailFragmentArgs by navArgs()

    private val attachmentAdapter: AttachmentAdapter by lazy {
        AttachmentAdapter(viewModel::onClickAttachmentItem)
    }

    private val pickAttachment =
        registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
            if (uri != null) {
                viewModel.uploadFile(FileUtils.getPathFromUri(requireContext(), uri))
            } else {
                DialogUtils.showDialog(
                    requireContext(),
                    title = "ERROR",
                    textColorTitle = R.color.red,
                    description = "Pick image error"
                )
            }
        }

    override fun initView() {
        binding.viewModel = viewModel
        binding.rcvAttachFile.adapter = attachmentAdapter
        viewModel.getTask(args.task.taskId!!)
        viewModel.getCategories()
    }

    override fun observer() {
        viewModel.categories.observe(viewLifecycleOwner) { categories ->
            var currentCategoryName = categories[0].name
            val categoryNames = categories.map {
                if (it.categoryId == args.task.categoryId) {
                    currentCategoryName = it.name
                }
                it.name
            }
            val categoryAdapter =
                ArrayAdapter(requireContext(), R.layout.item_textview, categoryNames)
            setCategoryAdapter(categoryAdapter, currentCategoryName)
        }

        viewModel.attachments.observe(viewLifecycleOwner) {
            attachmentAdapter.submitList(it)
        }
    }

    private fun setCategoryAdapter(adapter: ArrayAdapter<String>, initValue: String) {
        (binding.tilCategory.editText as? AutoCompleteTextView)?.apply {
            setText(initValue)
            setAdapter(adapter)
        }
    }

    override fun onStateError() {
        super.onStateError()
        viewModel.errorMessage?.let { AppToast.show(it) }
    }

    override fun showDatePicker(selectedDate: LocalDate?, onDateSelected: (LocalDate) -> Unit) {
        val _selectedDate =
            selectedDate?.atTime(LocalTime.now())?.atZone(ZoneId.systemDefault())?.toInstant()
                ?.toEpochMilli() ?: MaterialDatePicker.todayInUtcMilliseconds()
        val datePicker =
            MaterialDatePicker.Builder.datePicker()
                .setSelection(_selectedDate)
                .build()

        datePicker.isCancelable = false
        datePicker.addOnPositiveButtonClickListener {
            onDateSelected(Instant.ofEpochMilli(it).atZone(ZoneId.systemDefault()).toLocalDate())
        }
        datePicker.show(childFragmentManager, null)
    }

    override fun showTimePicker(selectedTime: LocalTime?, onTimeSelected: (LocalTime) -> Unit) {
        val _selectedTime = selectedTime ?: LocalTime.now()
        val timePicker = MaterialTimePicker.Builder()
            .setTimeFormat(CLOCK_24H)
            .setHour(_selectedTime.hour)
            .setMinute(_selectedTime.minute)
            .setTitleText(R.string.setting_time)
            .build()

        timePicker.isCancelable = false
        timePicker.addOnPositiveButtonClickListener {
            onTimeSelected(LocalTime.of(timePicker.hour, timePicker.minute))
        }
        timePicker.show(childFragmentManager, null)
    }

    override fun pickAttachment() {
        pickAttachment.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
    }

    override fun getIntent(clazz: Class<*>): Intent {
        return Intent(requireContext(), clazz)
    }

    override fun getAlarmManager(): AlarmManager {
        return requireContext().getSystemService(Context.ALARM_SERVICE) as AlarmManager
    }

    override fun getBroadcast(requestCode: Int, intent: Intent, flags: Int): PendingIntent {
        return PendingIntent.getBroadcast(requireContext(), requestCode, intent, flags)
    }

}