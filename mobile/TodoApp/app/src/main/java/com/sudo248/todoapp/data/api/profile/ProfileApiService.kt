package com.sudo248.todoapp.data.api.profile

import com.sudo248.base_android_annotation.apiservice.ApiService
import com.sudo248.base_android_annotation.apiservice.EnableAuthentication
import com.sudo248.base_android_annotation.apiservice.logging_level.Level
import com.sudo248.base_android_annotation.apiservice.logging_level.LoggingLever
import com.sudo248.todoapp.BuildConfig
import com.sudo248.todoapp.data.SharedPrefKeys
import com.sudo248.todoapp.data.api.BaseResponse
import com.sudo248.todoapp.domain.entity.auth.Profile
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT

@ApiService(baseUrl = BuildConfig.BASE_URL)
@EnableAuthentication(SharedPrefKeys.TOKEN)
@LoggingLever(level = Level.BODY)
interface ProfileApiService {

    @GET("profiles")
    suspend fun getProfile(): Response<BaseResponse<Profile>>

    @POST("profiles")
    suspend fun postProfile(@Body profile: Profile): Response<BaseResponse<Profile>>

    @PUT("profiles")
    suspend fun putProfile(@Body profile: Profile): Response<BaseResponse<Profile>>
}