package com.sudo248.todoapp.data.api.mock

import android.accounts.Account
import com.google.gson.Gson
import com.sudo248.todoapp.data.api.auth.request.AccountRequest
import com.sudo248.todoapp.data.api.auth.response.TokenResponse
import com.sudo248.todoapp.domain.entity.auth.Profile
import com.sudo248.todoapp.domain.entity.category.Category
import com.sudo248.todoapp.domain.entity.task.Task
import com.sudo248.todoapp.domain.entity.task.TaskStatus
import java.time.LocalDate

object MockData {
    val categories: MutableList<Category> = mutableListOf()
    val tasks: MutableList<Task> = mutableListOf()
    val accounts = mutableMapOf<AccountRequest, TokenResponse>()
    val profiles = mutableMapOf<TokenResponse, Profile>()
    val gson = Gson()

    val categoryAll = Category(
        categoryId = -1,
        name = "Tất cả",
        color = "#739FED",
        isShow = true,
    )

    init {
        accounts.put(
            AccountRequest(
                email = "test@gmail.com",
                password = "123456",
                agreePolicy = true
            ),
            TokenResponse(token = "1")
        )
        profiles.put(
            TokenResponse(token = "1"),
            Profile(
                profileId = 1,
                firstName = "Anh",
                lastName = "Nguyen",
                dob = LocalDate.of(1998, 9, 16)
            )
        )
        categories.addAll(
            listOf(
                Category(
                    categoryId = 1,
                    name = "Công việc",
                    color = "#739FED",
                    isShow = true,
                ),
                Category(
                    categoryId = 2,
                    name = "Ngày sinh nhật",
                    color = "#739FED",
                    isShow = true,
                ),
                Category(
                    categoryId = 3,
                    name = "Cá nhân",
                    color = "#739FED",
                    isShow = true,
                ),
                Category(
                    categoryId = 4,
                    name = "Danh sách yêu thích",
                    color = "#739FED",
                    isShow = true,
                ),
            )
        )
        tasks.addAll(
            listOf(
                Task(
                    taskId = 1,
                    title = "Test 1",
                    createdDate = LocalDate.now(),
                    updatedDate = LocalDate.now(),
                    deadline = LocalDate.now(),
                    note = "Note 1"
                ),
                Task(
                    taskId = 2,
                    title = "Test 2",
                    createdDate = LocalDate.now(),
                    updatedDate = LocalDate.now(),
                    deadline = LocalDate.now(),
                    note = "Note 2"
                ),
                Task(
                    taskId = 3,
                    title = "Test 3",
                    createdDate = LocalDate.now(),
                    updatedDate = LocalDate.now(),
                    deadline = LocalDate.now(),
                    status = TaskStatus.DONE,
                )
            )
        )
    }
}