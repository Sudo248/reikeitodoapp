package com.sudo248.todoapp.data.api

open class BaseResponse<Data> {
    var success: Boolean = false
    var message: String = ""
    var data: Data? = null

    constructor(success: Boolean, message: String, data: Data?) {
        this.success = success
        this.message = message
        this.data = data
    }

    override fun toString(): String {
        return "{" +
                "success: $success\n" +
                "message: $message\n" +
                "data: $data" +
                "}"
    }
}