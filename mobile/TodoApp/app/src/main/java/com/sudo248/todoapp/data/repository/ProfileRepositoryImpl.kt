package com.sudo248.todoapp.data.repository

import com.sudo248.base_android.core.DataState
import com.sudo248.base_android.data.api.handleResponse
import com.sudo248.base_android.ktx.stateOn
import com.sudo248.todoapp.data.api.profile.ProfileApiService
import com.sudo248.todoapp.domain.entity.auth.Profile
import com.sudo248.todoapp.domain.repository.ProfileRepository
import com.sudo248.todoapp.ktx.data
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProfileRepositoryImpl @Inject constructor(
    private val profileApiService: ProfileApiService,
    private val ioDispatcher: CoroutineDispatcher
) : ProfileRepository {
    override suspend fun getProfile(): DataState<Profile, Exception> =
        stateOn(ioDispatcher) {
            val response = handleResponse(profileApiService.getProfile())
            response.data()
        }

    override suspend fun createProfile(profile: Profile): DataState<Profile, Exception> =
        stateOn(ioDispatcher) {
            val response = handleResponse(profileApiService.postProfile(profile))
            response.data()
        }

    override suspend fun updateProfile(profile: Profile): DataState<Profile, Exception> =
        stateOn(ioDispatcher) {
            val response = handleResponse(profileApiService.putProfile(profile))
            response.data()
        }
}