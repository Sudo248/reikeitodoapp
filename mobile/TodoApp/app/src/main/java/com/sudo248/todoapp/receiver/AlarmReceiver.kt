package com.sudo248.todoapp.receiver

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import com.sudo248.todoapp.R
import com.sudo248.todoapp.domain.Constants
import com.sudo248.todoapp.ui.activity.main.MainActivity

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val notification: Notification = createNotification(
            context,
            Constants.NOTIFICATION_CHANNEL_ID,
            intent.getStringExtra(Constants.KEY_TITLE_NOTIFICATION) ?: "Remind",
            intent.getStringExtra(Constants.KEY_CONTENT_NOTIFICATION) ?: "Don't forget task",
            R.drawable.ic_notifications_active,
            getPendingIntentOpenMainActivity(context)
        )
        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.notify(Constants.NOTIFICATION_ID, notification)
    }

    private fun createNotification(
        context: Context,
        channelId: String,
        title: String,
        content: String,
        icon: Int,
        pendingIntent: PendingIntent? = null
    ): Notification {
        val builder = NotificationCompat.Builder(context, channelId).apply {
            pendingIntent?.let {
                setContentIntent(it)
            }
            setContentTitle(title)
            setContentText(content)
            setSmallIcon(icon)
            setDefaults(NotificationCompat.DEFAULT_SOUND)
            color = context.getColor(R.color.primaryColor)
            setCategory(NotificationCompat.CATEGORY_ALARM)
            setStyle(NotificationCompat.BigTextStyle())
        }
        return builder.build()
    }

    private fun getPendingIntentOpenMainActivity(context: Context) = PendingIntent.getActivity(
        context,
        5,
        Intent(context, MainActivity::class.java),
        PendingIntent.FLAG_UPDATE_CURRENT
    )
}