package com.sudo248.todoapp.data.repository

import com.sudo248.base_android.core.DataState
import com.sudo248.base_android.data.api.handleResponse
import com.sudo248.base_android.ktx.stateOn
import com.sudo248.todoapp.data.api.task.AttachmentApiService
import com.sudo248.todoapp.domain.entity.attachment.AttachFile
import com.sudo248.todoapp.domain.repository.AttachmentRepository
import com.sudo248.todoapp.ktx.data
import kotlinx.coroutines.CoroutineDispatcher
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AttachmentRepositoryImpl @Inject constructor(
    private val attachmentApiService: AttachmentApiService,
    private val ioDispatcher: CoroutineDispatcher
) : AttachmentRepository {

    override suspend fun uploadAttachment(
        taskId: Int,
        pathFile: String
    ): DataState<AttachFile, Exception> = stateOn(ioDispatcher) {
        val file = File(pathFile)
        val requestBody = file.asRequestBody("image/${file.extension}".toMediaTypeOrNull())
        val filePart = MultipartBody.Part.createFormData("file", file.name, requestBody)
        val response = handleResponse(attachmentApiService.uploadAttachment(taskId, filePart))
        response.data()
    }

    override suspend fun deleteAttachment(attachmentId: String): DataState<Unit, Exception> = stateOn(ioDispatcher) {
        val response = handleResponse(attachmentApiService.deleteAttachment(attachmentId))
        response.data()
    }
}