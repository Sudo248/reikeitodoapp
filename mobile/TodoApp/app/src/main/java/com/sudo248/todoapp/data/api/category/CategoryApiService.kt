package com.sudo248.todoapp.data.api.category

import com.sudo248.base_android_annotation.apiservice.ApiService
import com.sudo248.base_android_annotation.apiservice.AuthenticationRequired
import com.sudo248.base_android_annotation.apiservice.EnableAuthentication
import com.sudo248.base_android_annotation.apiservice.logging_level.Level
import com.sudo248.base_android_annotation.apiservice.logging_level.LoggingLever
import com.sudo248.todoapp.BuildConfig
import com.sudo248.todoapp.data.SharedPrefKeys
import com.sudo248.todoapp.data.api.BaseResponse
import com.sudo248.todoapp.domain.entity.category.Category
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

@ApiService(baseUrl = BuildConfig.BASE_URL)
@EnableAuthentication(SharedPrefKeys.TOKEN)
@LoggingLever(level = Level.BODY)
interface CategoryApiService {
    @GET("categories/")
    suspend fun getCategories(@Query("needCategoryAll") needCategoryAll: Boolean): Response<BaseResponse<List<Category>>>

    @GET("categories/{categoryId}")
    suspend fun getCategory(@Path("categoryId") categoryId: Int): Response<BaseResponse<Category>>

    @POST("categories/")
    suspend fun postCategory(@Body category: Category): Response<BaseResponse<Category>>

    @PUT("categories/{categoryId}")
    suspend fun putCategory(@Path("categoryId") categoryId: Int, @Body category: Category): Response<BaseResponse<Category>>

    @DELETE("categories/{categoryId}")
    suspend fun deleteCategory(@Path("categoryId") categoryId: Int): Response<BaseResponse<Any>>
}