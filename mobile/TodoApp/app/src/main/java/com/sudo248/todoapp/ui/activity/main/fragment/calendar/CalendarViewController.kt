package com.sudo248.todoapp.ui.activity.main.fragment.calendar

import androidx.annotation.StringRes
import com.sudo248.base_android.base.BaseViewController
import java.time.LocalDate

interface CalendarViewController : BaseViewController {
    fun getString(@StringRes id: Int): String
}