package com.sudo248.todoapp.domain.repository

import com.sudo248.base_android.core.DataState
import com.sudo248.todoapp.domain.entity.task.Task
import com.sudo248.todoapp.domain.entity.task.TaskSortCondition
import com.sudo248.todoapp.domain.entity.task.TaskStatus
import java.time.LocalDate

interface TaskRepository {
    suspend fun getTasks(categoryId: Int? = null): DataState<List<Task>, Exception>
    suspend fun getTasksByDeadline(deadline: LocalDate): DataState<List<Task>, Exception>
    suspend fun getTasksByStatus(status: TaskStatus): DataState<List<Task>, Exception>
    suspend fun getTaskById(taskId: Int): DataState<Task, Exception>
    suspend fun createNewTask(task: Task): DataState<Task, Exception>
    suspend fun updateTasks(task: Task): DataState<Task, Exception>
    suspend fun deleteTask(taskId: Int): DataState<Unit, Exception>
    fun getTaskSortCondition(): TaskSortCondition
    fun setTaskSortCondition(condition: TaskSortCondition)
}