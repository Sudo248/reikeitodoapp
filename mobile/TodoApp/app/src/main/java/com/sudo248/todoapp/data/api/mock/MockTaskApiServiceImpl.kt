package com.sudo248.todoapp.data.api.mock

import com.sudo248.todoapp.data.api.BaseResponse
import com.sudo248.todoapp.data.api.task.TaskApiService
import com.sudo248.todoapp.domain.entity.task.Task
import com.sudo248.todoapp.domain.entity.task.TaskStatus
import kotlinx.coroutines.delay
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Query
import java.time.LocalDate

class MockTaskApiServiceImpl : TaskApiService {

    private val tasks: List<Task>
        get() = mutableListOf<Task>().apply {
            addAll(MockData.tasks)
            addAll(MockData.categories.flatMap { it.tasks.orEmpty() })
        }


    override suspend fun getTasks(categoryId: Int?): Response<BaseResponse<List<Task>>> {
        delay(1000)
        return MockData.categories.firstOrNull { it.categoryId == categoryId }?.let {
            Response.success(
                200,
                BaseResponse(
                    success = true,
                    message = "Success",
                    data = it.tasks.orEmpty()
                )
            )
        } ?: Response.success(
            200,
            BaseResponse(
                success = true,
                message = "Success",
                data = tasks
            )
        )
    }

    override suspend fun getTaskById(taskId: Int): Response<BaseResponse<Task>> {
        delay(1000)
        return tasks.firstOrNull { it.taskId == taskId }?.let {
            Response.success(
                200,
                BaseResponse(
                    success = true,
                    message = "Success",
                    data = it
                )
            )
        } ?: Response.error(
            404, ResponseBody.create(
                null, MockData.gson.toJson(
                    BaseResponse(
                        success = false,
                        message = "Not found task",
                        data = null,
                    )
                )
            )
        )
    }

    override suspend fun postTask(task: Task): Response<BaseResponse<Task>> {
        delay(1000)
        val maxTaskId = MockData.tasks.maxOf { it.taskId ?: 0 }
        val newCategory = task.copy(taskId = maxTaskId + 1)
        MockData.tasks.add(newCategory)
        return Response.success(
            201,
            BaseResponse(
                success = true,
                message = "Success",
                data = newCategory
            )
        )
    }

    override suspend fun putTask(taskId: Int, task: Task): Response<BaseResponse<Task>> {
        delay(1000)
        val index = MockData.tasks.indexOfFirst { it.taskId == task.taskId }
        MockData.tasks[index] = task.copy()
        return Response.success(
            200,
            BaseResponse(
                success = true,
                message = "Success",
                data = MockData.tasks[index]
            )
        )
    }

    override suspend fun deleteTask(taskId: Int): Response<BaseResponse<Any>> {
        delay(1000)
        val isDeleteTask = MockData.tasks.removeIf { it.taskId == taskId }
        return if (isDeleteTask) {
            Response.success(
                200,
                BaseResponse(
                    success = true,
                    message = "Success",
                    data = taskId
                )
            )
        } else {
            Response.error(
                404, ResponseBody.create(
                    null, MockData.gson.toJson(
                        BaseResponse(
                            success = false,
                            message = "Not found task",
                            data = null,
                        )
                    )
                )
            )
        }

    }
}