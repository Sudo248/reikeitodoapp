package com.sudo248.todoapp.ui.activity.splash

import com.sudo248.base_android.base.BaseViewModel
import com.sudo248.base_android.ktx.createActionIntentDirections
import com.sudo248.base_android.navigation.IntentDirections
import com.sudo248.todoapp.ui.activity.auth.AuthActivity
import com.sudo248.todoapp.ui.activity.main.MainActivity
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel : BaseViewModel<IntentDirections>() {
    init {
        launch {
            delay(1000)
            navigator.navigateOff(AuthActivity::class.createActionIntentDirections())
//            navigator.navigateOff(MainActivity::class.createActionIntentDirections())
        }
    }
}