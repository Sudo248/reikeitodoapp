package com.sudo248.todoapp.ktx

import com.sudo248.base_android.data.exception.ApiException
import com.sudo248.todoapp.data.api.BaseResponse

fun ApiException.Companion.fromResponse(response: BaseResponse<*>): ApiException {
    return ApiException(
        statusCode = 0,
        message = response.message,
        data = response.data
    )
}

fun ApiException.errorBody(): ApiException {
    return if (data != null && data is BaseResponse<*>) {
        val errorBody = data as BaseResponse<*>
        ApiException.fromResponse(errorBody)
    } else {
        this
    }
}