package com.sudo248.todoapp.ui.activity.main.fragment.listcategory

import androidx.annotation.StringRes
import com.sudo248.base_android.base.BaseViewController
import com.sudo248.todoapp.domain.entity.category.Category

interface ListCategoryViewController : BaseViewController {
    fun getString(@StringRes id: Int): String
    fun getString(@StringRes id: Int, vararg format: Any): String
    fun showDialogCreateCategory(category: Category? = null, onSave: (Category) -> Unit)
}