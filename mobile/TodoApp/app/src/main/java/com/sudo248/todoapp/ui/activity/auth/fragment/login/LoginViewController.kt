package com.sudo248.todoapp.ui.activity.auth.fragment.login

import com.sudo248.base_android.base.BaseViewController
import com.sudo248.base_android.navigation.IntentDirections

interface LoginViewController : BaseViewController {
    fun navigateToActivity(directions: IntentDirections)
}