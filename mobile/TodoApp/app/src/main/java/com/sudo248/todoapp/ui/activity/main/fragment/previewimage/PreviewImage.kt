package com.sudo248.todoapp.ui.activity.main.fragment.previewimage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.sudo248.todoapp.databinding.FragmentPreviewImageBinding
import com.sudo248.todoapp.ui.uimodel.loadImage

class PreviewImage : Fragment() {
    private lateinit var binding: FragmentPreviewImageBinding
    private val args: PreviewImageArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPreviewImageBinding.inflate(layoutInflater, container, false)
        loadImage(binding.img, args.imageUrl)
        binding.imgBack.setOnClickListener {
            findNavController().popBackStack()
        }
        return binding.root
    }
}