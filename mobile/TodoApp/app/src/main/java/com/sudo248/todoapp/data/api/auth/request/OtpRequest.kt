package com.sudo248.todoapp.data.api.auth.request

data class OtpRequest(
    val email: String,
    val otp: Int
)