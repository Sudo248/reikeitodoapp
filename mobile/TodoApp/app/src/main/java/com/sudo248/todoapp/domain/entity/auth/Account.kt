package com.sudo248.todoapp.domain.entity.auth

import com.google.gson.annotations.SerializedName

data class Account(
    @SerializedName("id")
    val accountId: Int,
    val email: String,
    val profile: Profile
)
