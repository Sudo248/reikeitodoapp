package com.sudo248.todoapp.domain.repository

import com.sudo248.base_android.core.DataState
import com.sudo248.todoapp.domain.entity.attachment.AttachFile

interface AttachmentRepository {
    suspend fun uploadAttachment(taskId: Int, pathFile: String): DataState<AttachFile, Exception>
    suspend fun deleteAttachment(attachmentId: String): DataState<Unit, Exception>
}