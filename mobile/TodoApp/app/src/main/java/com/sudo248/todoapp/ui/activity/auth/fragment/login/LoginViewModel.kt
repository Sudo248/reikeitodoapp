package com.sudo248.todoapp.ui.activity.auth.fragment.login

import androidx.databinding.ObservableField
import androidx.navigation.NavDirections
import com.sudo248.base_android.ktx.createActionIntentDirections
import com.sudo248.base_android.ktx.onError
import com.sudo248.base_android.ktx.onSuccess
import com.sudo248.todoapp.data.api.auth.request.AccountRequest
import com.sudo248.todoapp.domain.repository.AuthRepository
import com.sudo248.todoapp.ui.activity.main.BaseAppViewModel
import com.sudo248.todoapp.ui.activity.main.MainActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val authRepository: AuthRepository
) : BaseAppViewModel<NavDirections>() {

    val edtEmail = ObservableField<String>()
    val edtPassword = ObservableField<String>()

    fun login() = bindUiState {
        authRepository.login(
            AccountRequest(
                email = edtEmail.get().orEmpty(),
                password = edtPassword.get().orEmpty()
            )
        )
            .onSuccess {
                viewController<LoginViewController>()
                    .navigateToActivity(MainActivity::class.createActionIntentDirections())
            }
    }

    fun navigateToSignIn() {
        navigator.navigateTo(LoginFragmentDirections.actionLoginFragmentToSignUpFragment())
    }
}