package com.sudo248.todoapp.ui.activity.main.fragment.listcategory

import androidx.fragment.app.viewModels
import com.sudo248.base_android.base.BaseFragment
import com.sudo248.base_android.utils.AppToast
import com.sudo248.todoapp.databinding.FragmentListCategoryBinding
import com.sudo248.todoapp.domain.entity.category.Category
import com.sudo248.todoapp.ui.activity.main.adapter.CategoryDetailAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListCategoryFragment : BaseFragment<FragmentListCategoryBinding, ListCategoryViewModel>(), ListCategoryViewController {
    override val viewModel: ListCategoryViewModel by viewModels()
    override val enableStateScreen: Boolean
        get() = true

    private val categoryAdapter: CategoryDetailAdapter by lazy {
        CategoryDetailAdapter(viewModel::onClickItemMenu)
    }

    override fun initView() {
        binding.viewModel = viewModel
        binding.rcvCategory.adapter = categoryAdapter
        viewModel.getAllCategory()
    }

    override fun observer() {
        viewModel.categories.observe(viewLifecycleOwner) {
            categoryAdapter.submitList(it.toList())
        }
    }

    override fun showDialogCreateCategory(category: Category?, onSave: (Category) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun onStateError() {
        super.onStateError()
        viewModel.errorMessage?.let { AppToast.show(it) }
    }
}