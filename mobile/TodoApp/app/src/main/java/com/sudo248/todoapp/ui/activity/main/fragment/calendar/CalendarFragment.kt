package com.sudo248.todoapp.ui.activity.main.fragment.calendar

import androidx.fragment.app.viewModels
import com.sudo248.base_android.base.BaseFragment
import com.sudo248.base_android.utils.AppToast
import com.sudo248.todoapp.databinding.FragmentCalendarBinding
import com.sudo248.todoapp.ui.activity.main.adapter.TaskAdapter
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDate
import java.time.ZoneId

@AndroidEntryPoint
class CalendarFragment : BaseFragment<FragmentCalendarBinding, CalendarViewModel>(),
    CalendarViewController {

    override val viewModel: CalendarViewModel by viewModels()

    private val taskAdapter: TaskAdapter by lazy {
        TaskAdapter(viewModel::onClickItemTask)
    }

    override fun initView() {
        binding.viewModel = viewModel
        binding.rcvManage.adapter = taskAdapter
        binding.calenderr.date =
            viewModel.currentSelectedDate.atStartOfDay(ZoneId.systemDefault()).toInstant()
                .toEpochMilli()
        viewModel.getTasksByDeadline(viewModel.currentSelectedDate)
        binding.calenderr.setOnDateChangeListener { _, year, month, dayOfMonth ->
            viewModel.currentSelectedDate = LocalDate.of(year, month + 1, dayOfMonth)
            viewModel.getTasksByDeadline(viewModel.currentSelectedDate)
        }
    }

    override fun observer() {
        viewModel.tasks.observe(viewLifecycleOwner) {
            if (it != null) {
                taskAdapter.submitList(it)
            }
        }
    }

    override fun onStateError() {
        super.onStateError()
        viewModel.errorMessage?.let { AppToast.show(it) }
    }
}