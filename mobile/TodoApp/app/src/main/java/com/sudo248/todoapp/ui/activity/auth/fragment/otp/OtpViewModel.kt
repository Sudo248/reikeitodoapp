package com.sudo248.todoapp.ui.activity.auth.fragment.otp

import android.os.CountDownTimer
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.sudo248.base_android.core.UiState
import com.sudo248.base_android.core.event.SingleEvent
import com.sudo248.base_android.ktx.onSuccess
import com.sudo248.base_android.utils.DateUtils
import com.sudo248.todoapp.data.api.auth.request.OtpRequest
import com.sudo248.todoapp.domain.Constants
import com.sudo248.todoapp.domain.repository.AuthRepository
import com.sudo248.todoapp.ui.activity.main.BaseAppViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class OtpViewModel @Inject constructor(
    private val authRepository: AuthRepository
) : BaseAppViewModel<NavDirections>() {

    val edtOtp = ObservableField<String>()
    lateinit var email: String

    private val _timeout = MutableLiveData<String>()
    val timeout: LiveData<String> = _timeout

    private val countDownTimer = object : CountDownTimer(Constants.TIMEOUT_OTP, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            _timeout.postValue(DateUtils.format_mm_ss(millisUntilFinished))
        }

        override fun onFinish() {
            _timeout.postValue("timeout")
        }
    }

    fun startTimer() {
        countDownTimer.cancel()
        countDownTimer.start()
    }

    fun submitOtp() {
        if (edtOtp.get().isNullOrEmpty() || edtOtp.get()?.length != 4) {
            error = SingleEvent(Exception("Invalid otp"))
            setState(UiState.ERROR)
        } else {
            onFullFillListener(edtOtp.get().orEmpty())
        }
    }

    fun onFullFillListener(otp: String) = bindUiState {
        countDownTimer.cancel()
        authRepository.confirmOtp(OtpRequest(email, otp.toInt())).onSuccess {
            navigator.navigateOff(OtpFragmentDirections.actionOtpFragmentToFulFillFragment())
        }
    }

    fun back() = navigator.back()

    override fun onCleared() {
        super.onCleared()
        countDownTimer.cancel()
    }

}
