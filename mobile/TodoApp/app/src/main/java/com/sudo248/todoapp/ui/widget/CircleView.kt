package com.sudo248.todoapp.ui.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.annotation.ColorInt
import androidx.core.view.setMargins
import androidx.recyclerview.widget.RecyclerView

class CircleView
@JvmOverloads
constructor(
    context: Context,
    attrs: AttributeSet? = null
) : View(context, attrs) {
    private val paint: Paint = Paint()
    private val borderPaint: Paint = Paint()

    init {
        val width = attrs?.getAttributeIntValue(android.R.attr.layout_width, 100) ?: 100
        val height = attrs?.getAttributeIntValue(android.R.attr.layout_height, 100) ?: 100
        val margin = attrs?.getAttributeIntValue(android.R.attr.layout_margin, 10) ?: 10
        layoutParams = RecyclerView.LayoutParams(width, height).apply {
            setMargins(margin)
        }
        val typedValue = TypedValue()
        context.theme.resolveAttribute(androidx.appcompat.R.attr.colorPrimary, typedValue, true)
        paint.color = typedValue.data
        borderPaint.color = Color.WHITE
    }

    fun setColor(@ColorInt color: Int) {
        paint.color = color
        invalidate()
    }

    fun setSize(size: Int) {
        layoutParams = RecyclerView.LayoutParams(size, size)
        requestLayout()
    }


    override fun onDraw(canvas: Canvas) {
        val x = width / 2f
        val y = height / 2f
        val radius = y
        canvas.drawCircle(x, y, radius, paint)
        if (isSelected) {
            canvas.drawCircle(x, y, radius - 3, borderPaint)
            canvas.drawCircle(x, y, radius - 9, paint)
        }
    }
}