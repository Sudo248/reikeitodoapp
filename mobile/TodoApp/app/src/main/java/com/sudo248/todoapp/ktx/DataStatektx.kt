package com.sudo248.todoapp.ktx

import com.sudo248.base_android.core.DataState
import com.sudo248.base_android.data.exception.ApiException
import com.sudo248.todoapp.data.api.BaseResponse

fun <Data> DataState<BaseResponse<Data>, ApiException>.data(): Data {
    if (isSuccess) {
        return get().data!!
    } else {
        val error = error()
        throw error.copy(message = error.message.takeIf { it.isNotEmpty() }
            ?: (error.data as? BaseResponse<*>)?.message ?: "Unexpected error")
    }
}