package com.sudo248.todoapp.ui.activity.main.adapter

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.sudo248.base_android.base.BaseListAdapter
import com.sudo248.base_android.base.BaseViewHolder
import com.sudo248.todoapp.R
import com.sudo248.todoapp.databinding.ItemTaskBinding
import com.sudo248.todoapp.domain.entity.task.Task
import com.sudo248.todoapp.domain.entity.task.TaskAction
import com.sudo248.todoapp.domain.entity.task.TaskStatus
import com.sudo248.todoapp.ui.activity.main.builder.PopupMenuBuilder
import com.sudo248.todoapp.ui.uimodel.AppMenuColor
import java.time.format.DateTimeFormatter

class TaskAdapter(
    private val onClickItem: (Task, TaskAction) -> Unit,
) : BaseListAdapter<Task, TaskAdapter.TaskViewHolder>() {
    private val formatDate = DateTimeFormatter.ofPattern("dd-MM")
    private val formatTime = DateTimeFormatter.ofPattern("HH:mm")

    override fun getViewHolder(
        layoutInflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<Task, *> {
        return TaskViewHolder(ItemTaskBinding.inflate(layoutInflater, parent, false))
    }

    inner class TaskViewHolder(binding: ItemTaskBinding) :
        BaseViewHolder<Task, ItemTaskBinding>(binding) {
        override fun onBind(item: Task) {
            binding.apply {
                setStatus(item.status)
                setFlagColor(item.color)
                imgAttachFile.isGone = item.attachments.isNullOrEmpty()
                imgNote.isGone = item.note.isNullOrEmpty()
                txtTitleTask.text = item.title
                txtDeadline.text = item.deadline.format(formatDate)
                imgAlert.isVisible = item.timeAlert != null
                txtTimeAlert.isVisible = item.timeAlert != null
                txtTimeAlert.text = item.timeAlert?.format(formatTime)
                setOnItemClickListener(item, onClickItem)
            }
        }

        private fun setStatus(status: TaskStatus) = binding.apply {
            if (status == TaskStatus.DONE) {
                cbDone.isChecked = true
                cbDone.buttonTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        root.context,
                        R.color.primaryColor
                    )
                )
                txtTitleTask.paintFlags = txtTitleTask.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                txtDeadline.setTextColor(ContextCompat.getColor(root.context, R.color.gray_75))
                txtTimeAlert.setTextColor(ContextCompat.getColor(root.context, R.color.gray_75))
            } else {
                cbDone.isChecked = false
                cbDone.buttonTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(
                        root.context,
                        R.color.gray_75
                    )
                )
                txtTitleTask.paintFlags =
                    txtTitleTask.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                txtDeadline.setTextColor(ContextCompat.getColor(root.context, R.color.red))
                txtTimeAlert.setTextColor(ContextCompat.getColor(root.context, R.color.red))
            }
        }

        private fun setFlagColor(color: String?) = binding.apply {
            if (color != null) {
                imgFlag.setImageResource(R.drawable.ic_flag)
                imgFlag.imageTintList =
                    ColorStateList.valueOf(Color.parseColor(color))
            } else {
                imgFlag.setImageResource(R.drawable.ic_outlined_flag)
                imgFlag.imageTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(root.context, R.color.gray_75))
            }
        }

        private fun setOnItemClickListener(
            item: Task,
            onItemClick: (Task, TaskAction) -> Unit
        ) = binding.apply {

            root.setOnClickListener {
                onItemClick(item, TaskAction.ACTION_CLICK_ITEM)
            }

            cbDone.setOnClickListener {
                onItemClick(
                    item.copy(status = if (item.status == TaskStatus.PROCESSING) TaskStatus.DONE else TaskStatus.PROCESSING),
                    TaskAction.ACTION_CHANGE_STATUS
                )
            }

            imgFlag.setOnClickListener {
                PopupMenuBuilder(it)
                    .setMenu(R.menu.popup_menu_color)
                    .setForceShowIcon(true)
                    .setOnItemMenuClickListener { menuItem ->
                        val colorFlag: AppMenuColor = when (menuItem.itemId) {
                            R.id.red -> AppMenuColor.RED
                            R.id.yellow -> AppMenuColor.YELLOW
                            R.id.purple -> AppMenuColor.PURPLE
                            R.id.blue -> AppMenuColor.BLUE
                            R.id.green -> AppMenuColor.GREEN
                            else -> AppMenuColor.CLEAR
                        }
                        onItemClick(
                            item.copy(color = colorFlag.value),
                            TaskAction.ACTION_CHANGE_FLAG
                        )
                    }
                    .build()
                    .show()
            }
        }
    }
}