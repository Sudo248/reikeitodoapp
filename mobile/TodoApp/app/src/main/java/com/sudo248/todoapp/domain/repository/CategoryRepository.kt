package com.sudo248.todoapp.domain.repository

import com.sudo248.base_android.core.DataState
import com.sudo248.todoapp.domain.entity.category.Category

interface CategoryRepository {
    suspend fun getAllCategories(needCategoryAll: Boolean = false): DataState<List<Category>, Exception>
    suspend fun createNewCategory(category: Category):  DataState<Category, Exception>
    suspend fun updateCategory(category: Category):  DataState<Category, Exception>
    suspend fun deleteCategory(categoryId: Int):  DataState<Unit, Exception>
}