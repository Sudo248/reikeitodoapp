package com.sudo248.todoapp.data.repository

import com.sudo248.base_android.core.DataState
import com.sudo248.base_android.data.api.handleResponse
import com.sudo248.base_android.data.local.shared_preference.SudoSharedPreference
import com.sudo248.base_android.ktx.stateOn
import com.sudo248.todoapp.data.SharedPrefKeys
import com.sudo248.todoapp.data.api.task.TaskApiService
import com.sudo248.todoapp.data.utils.boundTime
import com.sudo248.todoapp.domain.entity.task.Task
import com.sudo248.todoapp.domain.entity.task.TaskSortCondition
import com.sudo248.todoapp.domain.entity.task.TaskStatus
import com.sudo248.todoapp.domain.repository.TaskRepository
import com.sudo248.todoapp.ktx.data
import kotlinx.coroutines.CoroutineDispatcher
import java.time.LocalDate
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TaskRepositoryImpl @Inject constructor(
    private val taskApiService: TaskApiService,
    private val appPref: SudoSharedPreference,
    private val ioDispatcher: CoroutineDispatcher
) : TaskRepository {
    override suspend fun getTasks(categoryId: Int?): DataState<List<Task>, Exception> =
        stateOn(ioDispatcher) {
            val response = handleResponse(boundTime { taskApiService.getTasks(categoryId) })
            response.data()
        }

    override suspend fun getTasksByDeadline(deadline: LocalDate): DataState<List<Task>, Exception> =
        stateOn(ioDispatcher) {
            val response = handleResponse(taskApiService.getTasks())
            response.data().filter { it.deadline == deadline }
        }

    override suspend fun getTasksByStatus(status: TaskStatus): DataState<List<Task>, Exception> =
        stateOn(ioDispatcher) {
            val response = handleResponse(taskApiService.getTasks())
            response.data().filter { it.status == status }
        }

    override suspend fun getTaskById(taskId: Int): DataState<Task, Exception> =
        stateOn(ioDispatcher) {
            val response = handleResponse(boundTime { taskApiService.getTaskById(taskId) })
            response.data()
        }

    override suspend fun createNewTask(task: Task): DataState<Task, Exception> =
        stateOn(ioDispatcher) {
            val response = handleResponse(boundTime { taskApiService.postTask(task) })
            response.data()
        }

    override suspend fun updateTasks(task: Task): DataState<Task, Exception> =
        stateOn(ioDispatcher) {
            val response = handleResponse(boundTime { taskApiService.putTask(task.taskId!!, task) })
            response.data()
        }

    override suspend fun deleteTask(taskId: Int): DataState<Unit, Exception> =
        stateOn(ioDispatcher) {
            val response = handleResponse(boundTime { taskApiService.deleteTask(taskId) })
            response.data()
        }

    override fun getTaskSortCondition(): TaskSortCondition {
        return appPref.getObject(SharedPrefKeys.TASK_SORT_CONDITION, TaskSortCondition::class.java)
            ?: TaskSortCondition.SORT_BY_DEADLINE
    }

    override fun setTaskSortCondition(condition: TaskSortCondition) {
        appPref.putObject(SharedPrefKeys.TASK_SORT_CONDITION, condition)
    }
}