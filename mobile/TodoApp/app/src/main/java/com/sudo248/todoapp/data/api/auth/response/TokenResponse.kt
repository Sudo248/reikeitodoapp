package com.sudo248.todoapp.data.api.auth.response

data class TokenResponse(
    val token: String
)
