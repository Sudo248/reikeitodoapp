package com.sudo248.todoapp.data.api.task

import com.sudo248.base_android_annotation.apiservice.ApiService
import com.sudo248.base_android_annotation.apiservice.EnableAuthentication
import com.sudo248.base_android_annotation.apiservice.logging_level.Level
import com.sudo248.base_android_annotation.apiservice.logging_level.LoggingLever
import com.sudo248.todoapp.BuildConfig
import com.sudo248.todoapp.data.SharedPrefKeys
import com.sudo248.todoapp.data.api.BaseResponse
import com.sudo248.todoapp.domain.entity.task.Task
import com.sudo248.todoapp.domain.entity.task.TaskStatus
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query
import java.time.LocalDate

@ApiService(baseUrl = BuildConfig.BASE_URL)
@EnableAuthentication(SharedPrefKeys.TOKEN)
@LoggingLever(level = Level.BODY)
interface TaskApiService {
    @GET("tasks/")
    suspend fun getTasks(@Query("categoryId") categoryId: Int? = null): Response<BaseResponse<List<Task>>>

    @GET("tasks/{taskId}")
    suspend fun getTaskById(@Path("taskId") taskId: Int): Response<BaseResponse<Task>>

    @POST("tasks/")
    suspend fun postTask(@Body task: Task): Response<BaseResponse<Task>>

    @PUT("tasks/{taskId}")
    suspend fun putTask(@Path("taskId") taskId: Int, @Body task: Task): Response<BaseResponse<Task>>

    @DELETE("tasks/{taskId}")
    suspend fun deleteTask(@Path("taskId") taskId: Int): Response<BaseResponse<Any>>
}