package com.sudo248.todoapp.ui.uimodel

enum class AppMenuColor(val value: String?) {
    RED("#AD1457"),
    YELLOW("#FFB300"),
    PURPLE("#7B1FA2"),
    BLUE("#1565C0"),
    GREEN("#2E7D32"),
    CLEAR(null),
}