package com.sudo248.todoapp.data.api.mock

import com.sudo248.todoapp.data.api.BaseResponse
import com.sudo248.todoapp.data.api.category.CategoryApiService
import com.sudo248.todoapp.domain.entity.category.Category
import kotlinx.coroutines.delay
import okhttp3.ResponseBody
import retrofit2.Response

class MockCategoryApiServiceImpl : CategoryApiService {

    override suspend fun getCategories(needAll: Boolean): Response<BaseResponse<List<Category>>> {
        delay(1000)
        val categories = mutableListOf<Category>()
        if (needAll) {
            categories.add(
                MockData.categoryAll
            )
        }
        categories.addAll(MockData.categories)
        return Response.success(
            200,
            BaseResponse(
                success = true,
                message = "Success",
                data = categories
            )
        )
    }

    override suspend fun getCategory(categoryId: Int): Response<BaseResponse<Category>> {
        delay(1000)
        return MockData.categories.firstOrNull { it.categoryId == categoryId }?.let {
            Response.success(
                200,
                BaseResponse(
                    success = true,
                    message = "Success",
                    data = it
                )
            )
        } ?: Response.error(
            404, ResponseBody.create(
                null, MockData.gson.toJson(
                    BaseResponse(
                        success = false,
                        message = "Not found category",
                        data = null,
                    )
                )
            )
        )
    }

    override suspend fun postCategory(category: Category): Response<BaseResponse<Category>> {
        delay(1000)
        val maxCategoryId = MockData.categories.maxOf { it.categoryId ?: 0 }
        val newCategory = category.copy(categoryId = maxCategoryId + 1)
        MockData.categories.add(newCategory)
        return Response.success(
            201,
            BaseResponse(
                success = true,
                message = "Success",
                data = newCategory
            )
        )
    }

    override suspend fun putCategory(categoryId: Int, category: Category): Response<BaseResponse<Category>> {
        delay(1000)
        val index = MockData.categories.indexOfFirst { it.categoryId == category.categoryId }
        return if (index != -1) {
            MockData.categories[index] = category
            Response.success(
                200,
                BaseResponse(
                    success = true,
                    message = "Success",
                    data = category
                )
            )
        } else {
            Response.error(
                404, ResponseBody.create(
                    null, MockData.gson.toJson(
                        BaseResponse(
                            success = false,
                            message = "Not found category",
                            data = null,
                        )
                    )
                )
            )
        }
    }

    override suspend fun deleteCategory(categoryId: Int): Response<BaseResponse<Any>> {
        delay(1000)
        return if (MockData.categories.removeIf { it.categoryId == categoryId }) {
            Response.success(
                200,
                BaseResponse(
                    success = true,
                    message = "Success",
                    data = categoryId
                )
            )
        } else {
            Response.error(
                404, ResponseBody.create(
                    null, MockData.gson.toJson(
                        BaseResponse(
                            success = false,
                            message = "Not found category",
                            data = null,
                        )
                    )
                )
            )
        }
    }
}