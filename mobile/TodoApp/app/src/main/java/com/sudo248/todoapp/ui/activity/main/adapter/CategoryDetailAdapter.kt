package com.sudo248.todoapp.ui.activity.main.adapter

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.sudo248.base_android.base.BaseListAdapter
import com.sudo248.base_android.base.BaseViewHolder
import com.sudo248.todoapp.R
import com.sudo248.todoapp.databinding.ItemDetailCategoryBinding
import com.sudo248.todoapp.domain.entity.category.Category
import com.sudo248.todoapp.ui.activity.main.builder.PopupMenuBuilder

class CategoryDetailAdapter(
    private val onClickItemMenu: (view: View, Category, MenuItem) -> Unit
) : BaseListAdapter<Category, CategoryDetailAdapter.CategoryDetailViewHolder>() {

    override fun getViewHolder(
        layoutInflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<Category, *> {
        return CategoryDetailViewHolder(
            ItemDetailCategoryBinding.inflate(
                layoutInflater,
                parent,
                false
            )
        )
    }

    inner class CategoryDetailViewHolder(binding: ItemDetailCategoryBinding) :
        BaseViewHolder<Category, ItemDetailCategoryBinding>(binding) {
        override fun onBind(item: Category) {
            binding.apply {
                vColor.backgroundTintList = ColorStateList.valueOf(
                    item.color?.let { Color.parseColor(it) } ?: ContextCompat.getColor(
                        itemView.context,
                        R.color.primaryColor
                    )
                )
                txtName.text = item.name
                imgVisibilityOff.isGone = item.isShow
                txtTotalTask.text = "${item.countTasks}"
                imgMore.setOnClickListener { view ->
                    PopupMenuBuilder(view)
                        .setMenu(R.menu.popup_menu_category)
                        .applyItemAt(1) {
                            title = itemView.context.getString(
                                if (item.isShow) R.string.hide else R.string.show
                            )
                        }
                        .setOnItemMenuClickListener {
                            onClickItemMenu(view, item, it)
                        }
                        .build()
                        .show()
                }
            }
        }
    }
}