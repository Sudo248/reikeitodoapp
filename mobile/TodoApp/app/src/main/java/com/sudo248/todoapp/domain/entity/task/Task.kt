package com.sudo248.todoapp.domain.entity.task

import com.google.gson.annotations.SerializedName
import com.sudo248.base_android.base.ItemDiff
import com.sudo248.todoapp.domain.entity.attachment.AttachFile
import java.io.Serializable
import java.time.LocalDate
import java.time.LocalTime

data class Task(
    @SerializedName("id")
    val taskId: Int? = null,
    val categoryId: Int? = null,
    val title: String,
    val createdDate: LocalDate,
    val updatedDate: LocalDate,
    val deadline: LocalDate,
    val status: TaskStatus = TaskStatus.PROCESSING,
    val color: String? = null,
    val timeAlert: LocalTime? = null,
    val note: String? = null,
    @SerializedName("files")
    val attachments: List<AttachFile>? = null,
) : ItemDiff, Serializable {
    override fun isContentTheSame(other: ItemDiff): Boolean {
        return other is Task && this == other
    }

    override fun isItemTheSame(other: ItemDiff): Boolean {
        return other is Task &&
                this.taskId == other.taskId &&
                this.categoryId == other.categoryId &&
                this.title == other.title &&
                this.createdDate == other.createdDate &&
                this.deadline == other.deadline &&
                this.status == other.status &&
                this.color == other.color
    }

}
