package com.sudo248.todoapp.ui.activity.auth.fragment.fulfill

import com.sudo248.base_android.base.BaseViewController
import com.sudo248.base_android.navigation.IntentDirections

interface FulFillViewController : BaseViewController {
    fun navigateToActivity(directions: IntentDirections)
}