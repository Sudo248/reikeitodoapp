package com.sudo248.todoapp.ui.activity.main.fragment.listtask

import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.datepicker.MaterialDatePicker
import com.sudo248.base_android.base.BaseFragment
import com.sudo248.base_android.utils.AppToast
import com.sudo248.todoapp.R
import com.sudo248.todoapp.databinding.FragmentListTaskBinding
import com.sudo248.todoapp.ui.activity.main.adapter.CategoryAdapter
import com.sudo248.todoapp.ui.activity.main.adapter.TaskAdapter
import dagger.hilt.android.AndroidEntryPoint
import java.time.Instant
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId

@AndroidEntryPoint
class ListTaskFragment : BaseFragment<FragmentListTaskBinding, ListTaskViewModel>(), ListTaskViewController {

    override val viewModel: ListTaskViewModel by viewModels()

    override val enableStateScreen: Boolean = true

    private val categoryAdapter: CategoryAdapter by lazy {
        CategoryAdapter(viewModel::onClickItemCategory)
    }

    private val taskAdapter: TaskAdapter by lazy {
        TaskAdapter(viewModel::onClickItemTask)
    }

    override fun initView() {
        binding.viewModel = viewModel
        binding.rcvCategory.adapter = categoryAdapter
        binding.rcvTask.adapter = taskAdapter
        binding.refreshTask.setOnRefreshListener {
            viewModel.onRefreshTask()
        }
        setupSearchView()
    }

    override fun observer() {
        viewModel.categories.observe(viewLifecycleOwner) {
            categoryAdapter.submitData(it, viewModel.currentCategoryId)
        }

        viewModel.tasks.observe(viewLifecycleOwner) {
            taskAdapter.submitList(it)
        }

        lifecycleScope.launchWhenStarted {
            viewModel.uiStateTasks.collect {
                it.getValueIfNotHandled()?.let {  state ->
                    state.onState(
                        loading = {
                            binding.refreshTask.isRefreshing = true
                        },
                        success = {
                            binding.refreshTask.isRefreshing = false
                        },
                        error = {
                            binding.refreshTask.isRefreshing = false
                        }
                    )
                }
            }
        }
    }

    private fun setupSearchView() {
        binding.edtSearch.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.search(query.orEmpty())
                return true
            }

            override fun onQueryTextChange(newQuery: String?): Boolean {
                viewModel.onSearch(newQuery.orEmpty())
                return true
            }
        })

        binding.imgBack.setOnClickListener {
            binding.edtSearch.apply {
                setQuery("", true)
                clearFocus()
            }

            viewModel.exitSearchMode()
        }
    }

    override fun showDatePicker(selectedDate: LocalDate?, onDateSelected: (LocalDate) -> Unit) {
        val _selectedDate =
            selectedDate?.atTime(LocalTime.now())?.atZone(ZoneId.systemDefault())?.toInstant()
                ?.toEpochMilli() ?: MaterialDatePicker.todayInUtcMilliseconds()
        val datePicker =
            MaterialDatePicker.Builder.datePicker()
                .setTitleText(R.string.deadline)
                .setSelection(_selectedDate)
                .build()

        datePicker.isCancelable = false
        datePicker.addOnPositiveButtonClickListener {
            onDateSelected(Instant.ofEpochMilli(it).atZone(ZoneId.systemDefault()).toLocalDate())
        }
        datePicker.show(childFragmentManager, null)
    }

    override fun onStateError() {
        super.onStateError()
        viewModel.errorMessage?.let { AppToast.show(it) }
    }

}

