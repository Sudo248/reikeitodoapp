package com.sudo248.todoapp.ui.activity.main.fragment.calendar

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.sudo248.base_android.core.UiState
import com.sudo248.base_android.core.bus.SudoBus
import com.sudo248.base_android.core.event.SingleEvent
import com.sudo248.base_android.ktx.onSuccess
import com.sudo248.base_android.navigation.ResultCallback
import com.sudo248.todoapp.domain.entity.task.Task
import com.sudo248.todoapp.domain.entity.task.TaskAction
import com.sudo248.todoapp.domain.repository.TaskRepository
import com.sudo248.todoapp.ktx.getSerializableSafety
import com.sudo248.todoapp.ui.activity.main.BaseAppViewModel
import com.sudo248.todoapp.ui.activity.main.fragment.DataKeys
import com.sudo248.todoapp.ui.activity.main.fragment.EventKeys
import com.sudo248.todoapp.ui.activity.main.fragment.NavigationKeys
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.time.LocalDate
import javax.inject.Inject

@HiltViewModel
class CalendarViewModel @Inject constructor(
    private val taskRepository: TaskRepository
) : BaseAppViewModel<NavDirections>() {
    private val _tasks = MutableLiveData<List<Task>>()
    val tasks: LiveData<List<Task>> = _tasks

    private val _uiStateTasks = MutableStateFlow(SingleEvent(UiState.IDLE))
    val uiStateTasks: StateFlow<SingleEvent<UiState>> = _uiStateTasks

    var currentSelectedDate: LocalDate = LocalDate.now()

    fun getTasksByDeadline(deadline: LocalDate) {
        bindUiState {
            taskRepository.getTasksByDeadline(deadline).onSuccess {
                _tasks.postValue(it)
            }
        }
    }

    fun onClickItemTask(task: Task, action: TaskAction) = launch {
        when (action) {
            TaskAction.ACTION_CLICK_ITEM -> {
                navigator.navigateForResult(
                    CalendarFragmentDirections.actionCalendarFragmentToTaskDetailFragment(task),
                    NavigationKeys.ACTION_TO_DETAIL_FRAGMENT,
                    object : ResultCallback {
                        override fun onResult(key: String, data: Bundle?) {
                            if (key == NavigationKeys.ACTION_TO_DETAIL_FRAGMENT && data != null) {
                                handleResultFromTaskDetail(task, data)
                            }
                        }
                    }
                )
            }

            TaskAction.ACTION_CHANGE_FLAG, TaskAction.ACTION_CHANGE_STATUS -> {
                bindUiState(_uiStateTasks) {
                    taskRepository.updateTasks(task)
                        .onSuccess {
                            upsertTask(it)
                        }
                }
            }
        }
    }

    private fun handleResultFromTaskDetail(oldTask: Task, data: Bundle) {
        when {
            data.getBoolean(DataKeys.IS_UPDATE) -> {
                val updatedTask = data.getSerializableSafety<Task>(DataKeys.TASK)
                updatedTask?.let {
                    if (oldTask.categoryId != it.categoryId) {
                        deleteTask(oldTask)
                    }
                    upsertTask(updatedTask)
                }
            }

            data.getBoolean(DataKeys.IS_DELETE) -> {
                deleteTask(oldTask)
            }
        }
    }

    private fun upsertTask(task: Task) {
        val currentTasks = _tasks.value.orEmpty().toMutableList()
        val index = currentTasks.indexOfFirst { it.taskId == task.taskId }
        if (index != -1) {
            currentTasks[index] = task
            _tasks.postValue(currentTasks)
        }
        SudoBus.getDefaultInstance().publish(EventKeys.EVENT_UPDATE_TASK, task)
    }

    private fun deleteTask(task: Task) {
        val currentTasks = _tasks.value.orEmpty().toMutableList()
        currentTasks.removeIf { it.taskId == task.taskId }
        _tasks.postValue(currentTasks)
        SudoBus.getDefaultInstance().publish(EventKeys.EVENT_DELETE_TASK, task)
    }
}