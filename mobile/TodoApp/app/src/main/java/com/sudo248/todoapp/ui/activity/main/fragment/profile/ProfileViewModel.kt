package com.sudo248.todoapp.ui.activity.main.fragment.profile

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.sudo248.base_android.ktx.onSuccess
import com.sudo248.todoapp.domain.entity.auth.Profile
import com.sudo248.todoapp.domain.entity.task.TaskStatus
import com.sudo248.todoapp.domain.repository.ProfileRepository
import com.sudo248.todoapp.domain.repository.TaskRepository
import com.sudo248.todoapp.ui.activity.main.BaseAppViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import java.time.LocalDate

@HiltViewModel
class ProfileViewModel @javax.inject.Inject constructor(
    private val profileRepository: ProfileRepository,
    private val taskRepository: TaskRepository
) : BaseAppViewModel<NavDirections>() {

    private val _profile = MutableLiveData<Profile>()
    var profile: LiveData<Profile> = _profile

    private val _countTaskDone = MutableLiveData<Int>()
    var countTaskDone: LiveData<Int> = _countTaskDone

    private val _countTaskProcess = MutableLiveData<Int>()
    var countTaskProcess: LiveData<Int> = _countTaskProcess

    val spinnerDate = ObservableArrayList<Int>()
    val spinnerMonth = ObservableArrayList<Int>()
    val spinnerYear = ObservableArrayList<Int>()

    val edtFirstName = ObservableField<String>()
    val edtLastName = ObservableField<String>()

    val datePos = ObservableField<Int>()
    val monthPos = ObservableField<Int>()
    val yearPos = ObservableField<Int>()


    fun onClickUpdate() {
        profile.value?.let { profile ->
            val d: Int = spinnerDate[datePos.get()!!]
            val m: Int = spinnerMonth[monthPos.get()!!]
            val y: Int = spinnerYear[yearPos.get()!!]
            bindUiState {
                profileRepository.updateProfile(profile.copy(dob = LocalDate.of(y, m, d)))
                    .onSuccess {
                        _profile.postValue(it)
                    }
            }
        }
    }

    fun getProfile1() {
        bindUiState {
            profileRepository.getProfile().onSuccess {
                _profile.postValue(it)
            }
        }
    }

    fun countTask() {
        bindUiState {
            taskRepository.getTasksByStatus(TaskStatus.DONE).onSuccess {
                _countTaskDone.postValue(it.count())
            }
            taskRepository.getTasksByStatus(TaskStatus.PROCESSING).onSuccess {
                _countTaskProcess.postValue(it.count())
            }
        }
    }

    fun setSpinner() {
        for (i in 1..31) {
            spinnerDate.add(i)
        }
        for (i in 1..12) {
            spinnerMonth.add(i)
        }
        for (i in LocalDate.now().year.downTo(1900)) {
            spinnerYear.add(i)
        }
    }
}