package com.sudo248.todoapp.ui.activity.auth.fragment.login

import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.sudo248.base_android.base.BaseFragment
import com.sudo248.base_android.navigation.IntentDirections
import com.sudo248.base_android.utils.AppToast
import com.sudo248.base_android.utils.DialogUtils
import com.sudo248.todoapp.R
import com.sudo248.todoapp.databinding.FragmentLoginBinding
import com.sudo248.todoapp.ui.activity.auth.AuthActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding, LoginViewModel>(), LoginViewController {
    override val viewModel: LoginViewModel by viewModels()
    override val enableStateScreen: Boolean
        get() = true

    override fun initView() {
        binding.viewModel = viewModel
    }

    override fun onStateError() {
        super.onStateError()
        viewModel.errorMessage?.let {
            AppToast.show(it)
        }
    }

    override fun navigateToActivity(directions: IntentDirections) {
        (activity as AuthActivity).navigateTo(directions)
    }
}