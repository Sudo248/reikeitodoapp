package com.sudo248.todoapp.ui.activity.main.builder

import android.content.Context
import android.view.MenuItem
import android.view.View
import androidx.annotation.MenuRes
import androidx.appcompat.widget.PopupMenu
import androidx.core.view.get

class PopupMenuBuilder(private val context: Context) {

    constructor(view: View) : this(view.context) {
        this.anchor = view
    }

    private var anchor: View? = null

    @MenuRes
    private var menu: Int? = null

    private var onItemMenuClick: ((MenuItem) -> Unit)? = null

    private val itemChange: MutableList<Pair<Int, MenuItem.() -> Unit>> = mutableListOf()

    private var isShowIcon = false

    fun setAnchor(view: View) = apply {
        anchor = view
    }

    fun setMenu(@MenuRes menu: Int) = apply {
        this.menu = menu
    }

    fun setOnItemMenuClickListener(onItemMenuClick: (MenuItem) -> Unit) = apply {
        this.onItemMenuClick = onItemMenuClick
    }

    fun applyItemAt(position: Int, action: MenuItem.() -> Unit) = apply {
        this.itemChange.add(position to action)
    }

    fun setForceShowIcon(isShowIcon: Boolean) = apply {
        this.isShowIcon = isShowIcon
    }

    fun build(): PopupMenu {
        val anchor = checkNotNull(anchor) { "required anchor" }
        val menu = checkNotNull(menu) { "required menu" }
        val popupMenu = PopupMenu(context, anchor)
        popupMenu.menuInflater.inflate(menu, popupMenu.menu)
        itemChange.map {
            popupMenu.menu[it.first].apply(it.second)
        }
        popupMenu.setForceShowIcon(isShowIcon)
        popupMenu.setOnMenuItemClickListener {
            onItemMenuClick?.invoke(it)
            true
        }
        return popupMenu
    }

}