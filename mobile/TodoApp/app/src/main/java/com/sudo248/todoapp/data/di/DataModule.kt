package com.sudo248.todoapp.data.di

import android.content.Context
import androidx.core.content.ContextCompat
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.sudo248.base_android.data.api.ApiService
import com.sudo248.base_android.data.api.api
import com.sudo248.base_android.data.local.shared_preference.SudoSharedPreference
import com.sudo248.base_android.data.local.shared_preference.factory.SudoSharedPreferenceFactory
import com.sudo248.base_android.utils.AppSharedPreference
import com.sudo248.base_android_annotation.apiservice.ApiService
import com.sudo248.base_android_annotation.apiservice.logging_level.Level
import com.sudo248.todoapp.BuildConfig
import com.sudo248.todoapp.R
import com.sudo248.todoapp.data.api.auth.AuthApiService
import com.sudo248.todoapp.data.api.category.CategoryApiService
import com.sudo248.todoapp.data.api.mock.MockAuthApiServiceImpl
import com.sudo248.todoapp.data.api.mock.MockCategoryApiServiceImpl
import com.sudo248.todoapp.data.api.mock.MockProfileApiServiceImpl
import com.sudo248.todoapp.data.api.mock.MockTaskApiServiceImpl
import com.sudo248.todoapp.data.api.profile.ProfileApiService
import com.sudo248.todoapp.data.api.task.AttachmentApiService
import com.sudo248.todoapp.data.api.task.TaskApiService
import com.sudo248.todoapp.data.gson.LocalDateConverter
import com.sudo248.todoapp.data.gson.LocalTimeConverter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import java.time.LocalDate
import java.time.LocalTime
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Singleton
    @Provides
    fun provideAuthService(): AuthApiService =
        if(BuildConfig.IS_MOCK_DATA) MockAuthApiServiceImpl() else ApiService()

    @Singleton
    @Provides
    fun provideCategoryService(): CategoryApiService =
        if (BuildConfig.IS_MOCK_DATA) MockCategoryApiServiceImpl() else ApiService()

    @Singleton
    @Provides
    fun provideTaskService(): TaskApiService =
        if (BuildConfig.IS_MOCK_DATA) MockTaskApiServiceImpl() else api {
            converterFactory = GsonConverterFactory.create(
                GsonBuilder()
                    .registerTypeAdapter(LocalDate::class.java, LocalDateConverter())
                    .registerTypeAdapter(LocalTime::class.java, LocalTimeConverter())
                    .create()
            )
        }

    @Singleton
    @Provides
    fun provideAttachmentApiService(): AttachmentApiService = ApiService()

    @Singleton
    @Provides
    fun provideProfileApiService(): ProfileApiService =
        if (BuildConfig.IS_MOCK_DATA) MockProfileApiServiceImpl() else api {
        converterFactory = GsonConverterFactory.create(
            GsonBuilder()
                .registerTypeAdapter(LocalDate::class.java, LocalDateConverter())
                .registerTypeAdapter(LocalTime::class.java, LocalTimeConverter())
                .create()
        )
    }

    @Singleton
    @Provides
    fun provideAppSharePreference(@ApplicationContext context: Context): SudoSharedPreference {
        AppSharedPreference.create(context)
        return AppSharedPreference.get()
    }

    @Singleton
    @Provides
    fun provideIODispatcher() = Dispatchers.IO
}