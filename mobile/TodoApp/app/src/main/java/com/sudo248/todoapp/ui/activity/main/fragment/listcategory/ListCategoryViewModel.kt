package com.sudo248.todoapp.ui.activity.main.fragment.listcategory

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.sudo248.base_android.ktx.onSuccess
import com.sudo248.base_android.ktx.showWithTransparentBackground
import com.sudo248.base_android.utils.DialogUtils
import com.sudo248.todoapp.R
import com.sudo248.todoapp.domain.entity.category.Category
import com.sudo248.todoapp.domain.repository.CategoryRepository
import com.sudo248.todoapp.ui.activity.main.BaseAppViewModel
import com.sudo248.todoapp.ui.activity.main.fragment.DataKeys
import com.sudo248.todoapp.ui.activity.main.fragment.NavigationKeys
import com.sudo248.todoapp.ui.uimodel.AppMenuColor
import com.sudo248.todoapp.ui.widget.CircleView
import com.sudo248.todoapp.ui.widget.ColorPicker
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ListCategoryViewModel @Inject constructor(
    private val categoryRepository: CategoryRepository
) : BaseAppViewModel<NavDirections>() {

    private val _categories = MutableLiveData<List<Category>>()
    val categories: LiveData<List<Category>> = _categories

    private val controller: ListCategoryViewController
        get() = viewController()

    private var isUpdateCategory = false

    private val menuColors: List<String> =
        AppMenuColor.values().asList().filter { it.value != null }.map { it.value!! }


    fun getAllCategory() = bindUiState {
        categoryRepository.getAllCategories()
            .onSuccess {
                _categories.postValue(it)
            }
    }

    fun onClickItemMenu(view: View, category: Category, menuItem: MenuItem) {
        when (menuItem.itemId) {
            R.id.editCategory -> {
                showDialogUpsertCategory(view, category)
            }

            R.id.action -> {
                bindUiState {
                    categoryRepository.updateCategory(category.copy(isShow = !category.isShow))
                        .onSuccess {
                            upsertCategory(it)
                        }
                }
            }

            else -> {
                showPopupConfirmDeleteCategory(view, category)
            }
        }
    }

    fun onClickCreateCategory(view: View) {
        showDialogUpsertCategory(view)
    }

    private fun upsertCategory(category: Category) {
        isUpdateCategory = true
        val categories = _categories.value.orEmpty().toMutableList()
        val indexCategory = categories.indexOfFirst { it.categoryId == category.categoryId }
        if (indexCategory == -1) {
            categories.add(category)
        } else {
            categories[indexCategory] = category
        }
        _categories.postValue(categories)
    }

    private fun deleteCategory(category: Category) {
        isUpdateCategory = true
        val categories = _categories.value.orEmpty().toMutableList()
        categories.removeIf { it.categoryId == category.categoryId }
        _categories.postValue(categories)
    }

    private fun showDialogUpsertCategory(view: View, category: Category? = null) {
        val dialog = Dialog(view.context)
        val dialogView =
            LayoutInflater.from(view.context)
                .inflate(R.layout.dialog_create_category, view.parent as? ViewGroup, false)
        dialog.setContentView(dialogView)

        var currentCategoryColor: String? = category?.color ?: "#739FED"

        dialogView.apply {
            findViewById<TextView>(R.id.txtTitle).text =
                controller.getString(if (category != null) R.string.update_category else R.string.note)

            val edtInput = findViewById<EditText>(R.id.edtCategoryName)
            category?.let { edtInput.setText(it.name) }

            val currentColorView = findViewById<CircleView>(R.id.cvCurrentColor).apply {
                currentCategoryColor?.let { setColor(Color.parseColor(it)) }
            }

            findViewById<ColorPicker>(R.id.colorPicker).apply {
                setColors(menuColors)
                setInitColor(currentCategoryColor)
                setOnPickColorListener(object : ColorPicker.OnPickColorListener {
                    override fun onPickColor(color: Int, hexColor: String?) {
                        currentColorView.setColor(color)
                        currentCategoryColor = hexColor
                    }
                })
            }

            findViewById<TextView>(R.id.txtNegative).setOnClickListener {
                dialog.hide()
            }
            findViewById<TextView>(R.id.txtPositive).setOnClickListener {
                dialog.hide()
                bindUiState {
                    if (category == null) {
                        categoryRepository.createNewCategory(
                            Category(
                                name = edtInput.text.toString(),
                                color = currentCategoryColor,
                                isShow = true
                            )
                        )
                            .onSuccess {
                                upsertCategory(it)
                            }
                    } else {
                        categoryRepository.updateCategory(
                            category.copy(
                                name = edtInput.text.toString(),
                                color = currentCategoryColor
                            )
                        )
                            .onSuccess {
                                upsertCategory(it)
                            }
                    }

                }
            }
        }
        dialog.showWithTransparentBackground()

    }

    fun back() {
        val data = Bundle().apply {
            putBoolean(DataKeys.NEED_RELOAD, isUpdateCategory)
        }
        navigator.back(NavigationKeys.ACTION_TO_CATEGORY_FRAGMENT, data)
    }

    private fun showPopupConfirmDeleteCategory(view: View, category: Category) {
        DialogUtils.showConfirmDialog(
            context = view.context,
            title = controller.getString(R.string.delete_category, category.name),
            description = "",
            negative = controller.getString(R.string.cancel),
            positive = controller.getString(R.string.delete),
            onPositive = {
                bindUiState {
                    categoryRepository.deleteCategory(category.categoryId!!)
                        .onSuccess {
                            deleteCategory(category)
                        }
                }
            }
        )
    }

}