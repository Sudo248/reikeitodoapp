package com.sudo248.todoapp.domain

object Constants {
    const val TIMEOUT_OTP = 30_000L
    const val DEFAULT_BOUND_TIME = 500
    const val FORMAT_DATE = "dd/MM/yyyy"

    const val NOTIFICATION_CHANNEL_ID = "notification-channel"
    const val NOTIFICATION_CHANNEL_NAME = "Notification"
    const val NOTIFICATION_ID = 1

    const val KEY_TITLE_NOTIFICATION = "KEY_TITLE_NOTIFICATION"
    const val KEY_CONTENT_NOTIFICATION = "KEY_CONTENT_NOTIFICATION"
}