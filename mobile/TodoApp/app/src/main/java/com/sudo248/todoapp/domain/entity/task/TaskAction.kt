package com.sudo248.todoapp.domain.entity.task

enum class TaskAction {
    ACTION_CLICK_ITEM,
    ACTION_CHANGE_STATUS,
    ACTION_CHANGE_FLAG
}