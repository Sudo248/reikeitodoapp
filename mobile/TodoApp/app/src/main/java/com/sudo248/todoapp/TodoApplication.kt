package com.sudo248.todoapp

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import com.sudo248.base_android.app.BaseApplication
import com.sudo248.base_android.utils.AppSharedPreference
import com.sudo248.base_android.utils.AppToast
import com.sudo248.todoapp.domain.Constants
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TodoApplication : BaseApplication() {
    override fun onCreate() {
        super.onCreate()
        AppToast.create(this)
        AppSharedPreference.create(this)
        val notificationManager = getSystemService(NotificationManager::class.java)
        createNotificationChannel(
            notificationManager,
            Constants.NOTIFICATION_CHANNEL_ID,
            Constants.NOTIFICATION_CHANNEL_NAME,
            "Notification"
        )
    }

    private fun createNotificationChannel(
        manager: NotificationManager,
        channelId: String,
        channelName: String,
        description: String? = null
    ) {
        val channel = NotificationChannel(
            channelId,
            channelName,
            NotificationManager.IMPORTANCE_DEFAULT
        )
        channel.description = description
        manager.createNotificationChannel(channel)
    }

}