package com.sudo248.todoapp.data.api.mock

import android.media.session.MediaSession.Token
import com.sudo248.base_android.utils.AppSharedPreference
import com.sudo248.todoapp.data.SharedPrefKeys
import com.sudo248.todoapp.data.api.BaseResponse
import com.sudo248.todoapp.data.api.auth.response.TokenResponse
import com.sudo248.todoapp.data.api.profile.ProfileApiService
import com.sudo248.todoapp.domain.entity.auth.Profile
import kotlinx.coroutines.delay
import okhttp3.ResponseBody
import retrofit2.Response

class MockProfileApiServiceImpl: ProfileApiService {
    override suspend fun getProfile(): Response<BaseResponse<Profile>> {
        return MockData.profiles.keys.firstOrNull() { it.token == getToken()}?.let {
            Response.success(
                200,
                BaseResponse(
                    success = true,
                    message = "Success",
                    data = MockData.profiles.get(it)
                )
            )
        } ?: Response.error(
            404, ResponseBody.create(
                null, MockData.gson.toJson(
                    BaseResponse(
                        success = false,
                        message = "Token invalid",
                        data = null,
                    )
                )
            )
        )
    }

    override suspend fun postProfile(profile: Profile): Response<BaseResponse<Profile>> {
        TODO("Not yet implemented")
    }

    override suspend fun putProfile(profile: Profile): Response<BaseResponse<Profile>> {
        delay(1000)
        val p = MockData.profiles.values.firstOrNull() { it.profileId == profile.profileId }
        return if (p != null) {
            getToken()?.let { TokenResponse(token= it) }?.let { MockData.profiles.put(it, profile) }
            Response.success(
                200,
                BaseResponse(
                    success = true,
                    message = "Success",
                    data = profile
                )
            )
        } else {
            Response.error(
                404, ResponseBody.create(
                    null, MockData.gson.toJson(
                        BaseResponse(
                            success = false,
                            message = "Not found profile",
                            data = null,
                        )
                    )
                )
            )
        }
    }

    private fun getToken(): String? {
        return AppSharedPreference.I.getString(SharedPrefKeys.TOKEN, "")
    }
}