package com.sudo248.todoapp.domain.entity.task

enum class TaskSortCondition {
    SORT_BY_DEADLINE,
    SORT_BY_PRIORITY,
    SORT_BY_STATUS,
    SORT_BY_CREATE_TIME,
    SORT_BY_ALPHABET
}