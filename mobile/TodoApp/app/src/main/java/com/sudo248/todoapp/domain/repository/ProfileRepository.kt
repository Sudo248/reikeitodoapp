package com.sudo248.todoapp.domain.repository

import com.sudo248.base_android.core.DataState
import com.sudo248.todoapp.domain.entity.auth.Profile

interface ProfileRepository {
    suspend fun getProfile(): DataState<Profile, Exception>
    suspend fun createProfile(profile: Profile): DataState<Profile, Exception>
    suspend fun updateProfile(profile: Profile): DataState<Profile, Exception>
}