package com.sudo248.todoapp.domain.validate

class EmailValidate : Validate<String> {
    private val pattern = "^[\\w-\\.]+@([\\w-]+\\.)+[a-z]{2,4}\$"

    override fun validate(value: String): Boolean {
        return value.matches(pattern.toRegex())
    }
}