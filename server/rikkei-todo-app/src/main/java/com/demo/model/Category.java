package com.demo.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "categories")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "color")
    private String color;

    @Column(name = "isShow")
    private Boolean isShow;
    @ManyToOne(fetch = FetchType.LAZY)

    @JoinColumn(name = "account_id")
    private Account createdAccount;
}
