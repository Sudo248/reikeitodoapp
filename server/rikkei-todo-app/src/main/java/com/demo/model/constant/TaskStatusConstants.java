package com.demo.model.constant;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public enum TaskStatusConstants {
    NOT_DOING("NOT_DOING"),
    DONE("DONE"),
    PROCESSING("PROCESSING");

    public static final List<String> STATUS_LIST = new ArrayList<>();
    public static final String STATUS_LIST_STRING;

    static {
        for (TaskStatusConstants status : values()) {
            STATUS_LIST.add(status.getValue());
        }
        STATUS_LIST_STRING = String.join(", ", STATUS_LIST);
    }

    private final String value;

    TaskStatusConstants(String value) {
        this.value = value;
    }
}
