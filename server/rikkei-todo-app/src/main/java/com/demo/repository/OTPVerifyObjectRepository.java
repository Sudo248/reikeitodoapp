package com.demo.repository;

import com.demo.model.OTPVerifyObject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OTPVerifyObjectRepository extends JpaRepository<OTPVerifyObject, Integer> {
    Optional<OTPVerifyObject> findByEmail(String email);
}
