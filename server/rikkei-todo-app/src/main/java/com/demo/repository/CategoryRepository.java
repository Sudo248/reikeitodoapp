package com.demo.repository;

import com.demo.model.Account;
import com.demo.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Optional<Category> findByTitle(String title);

    List<Category> findAllByCreatedAccount(Account account);
}
