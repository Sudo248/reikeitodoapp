package com.demo.repository;

import com.demo.model.Account;
import com.demo.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Integer> {
    Optional<List<Task>> findByCategory_Id(Integer categoryId);

    Integer countByCategoryId(Integer categoryId);

    List<Task> findAllByAccount(Account account);
}
