package com.demo.repository;

import com.demo.model.Account;
import com.demo.model.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProfileRepository extends JpaRepository<Profile, Integer> {
    Optional<Profile> findByAccount(Account account);
}
