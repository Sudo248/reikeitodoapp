package com.demo.web.dto.request;

import lombok.Data;

@Data
public class VerifyOTPReq {
    private String email;
    private Integer otp;
}
