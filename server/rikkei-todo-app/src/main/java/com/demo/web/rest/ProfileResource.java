package com.demo.web.rest;

import com.demo.service.ProfileService;
import com.demo.web.dto.request.ProfileReq;
import com.demo.web.dto.response.utils.ResponseUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/profiles")
@CrossOrigin("*")
@RequiredArgsConstructor
public class ProfileResource {
    private final ProfileService profileService;

    @GetMapping
    public ResponseEntity<?> getProfile() {
        return ResponseUtils.created(profileService.getProfile());
    }

    @PostMapping
    public ResponseEntity<?> createProfile(@RequestBody ProfileReq profileReq) {
        return ResponseUtils.created(profileService.createProfile(profileReq));
    }

    @PutMapping
    public ResponseEntity<?> updateProfile(@RequestBody ProfileReq profileReq) {
        return ResponseUtils.ok("Updated", profileService.updateProfile(profileReq));
    }
}
