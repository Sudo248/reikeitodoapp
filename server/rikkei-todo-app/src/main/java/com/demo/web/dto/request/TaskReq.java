package com.demo.web.dto.request;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class TaskReq {
    private String title;
    private LocalDate deadline;
    private LocalTime timeAlert;
    private String note;
    private String color;
    private String status;
    private Integer categoryId;
}
