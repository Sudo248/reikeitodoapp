package com.demo.web.rest;

import com.demo.service.AttachmentsService;
import com.demo.web.dto.response.utils.ResponseUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

@RestController
@RequestMapping("/api/attachments")
@CrossOrigin("*")
@RequiredArgsConstructor
public class AttachmentsResource {
    private final AttachmentsService attachmentsService;

    @PostMapping("/upload/{taskId}")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable Integer taskId) {
        return ResponseUtils.ok(attachmentsService.uploadFile(taskId, file));
    }

    @GetMapping("/{fileId}/{fileName:.+}")
    @ResponseBody
    public ResponseEntity<?> getFile(@PathVariable String fileId, @PathVariable String fileName) {
        Resource file = attachmentsService.load(fileId, fileName);
        String lowercaseFilename = Objects.requireNonNull(file.getFilename()).toLowerCase();
        if (lowercaseFilename.endsWith(".jpg") ||
                lowercaseFilename.endsWith(".jpeg") ||
                lowercaseFilename.endsWith(".png") ||
                lowercaseFilename.endsWith(".gif") ||
                lowercaseFilename.endsWith(".bmp")) {
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_TYPE, "image/" + lowercaseFilename.substring(lowercaseFilename.lastIndexOf('.') + 1))
                    .body(file);
        }
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
                        + file.getFilename() + "\"").body(file);
    }

    @DeleteMapping("/{fileId}")
    public ResponseEntity<?> deleteAttachmentFile(@PathVariable String fileId) {
        return ResponseUtils.ok("Removed", attachmentsService.deleteAttachmentFile(fileId));
    }
}
