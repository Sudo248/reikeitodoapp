package com.demo.web.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

@Data
public class ProfileDto {
    private String email;
    private String firstName;
    private String lastName;
    private LocalDate dob;
}
