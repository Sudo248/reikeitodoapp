package com.demo.web.dto.request;

import lombok.Data;

@Data
public class CategoryReq {
    private String title;
    private String color;
    private Boolean isShow;
}
