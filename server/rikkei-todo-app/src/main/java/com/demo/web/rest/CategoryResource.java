package com.demo.web.rest;

import com.demo.service.CategoryService;
import com.demo.web.dto.request.CategoryReq;
import com.demo.web.dto.response.utils.ResponseUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/categories")
@CrossOrigin("*")
@RequiredArgsConstructor
public class CategoryResource {
    private final CategoryService categoryService;

    @GetMapping
    public ResponseEntity<?> getAllCategory(@RequestParam("needCategoryAll") boolean needCategoryAll) {
        return ResponseUtils.ok(categoryService.getAllCategory(needCategoryAll));
    }

    @GetMapping("/{categoryId}")
    public ResponseEntity<?> getCategoryById(@PathVariable Integer categoryId) {
        return ResponseUtils.ok(categoryService.getCategoryById(categoryId));
    }

    @PostMapping
    public ResponseEntity<?> createCategory(@RequestBody CategoryReq categoryReq) {
        return ResponseUtils.created(categoryService.createCategory(categoryReq));
    }

    @PutMapping("/{categoryId}")
    public ResponseEntity<?> updateCategory(@PathVariable Integer categoryId, @RequestBody CategoryReq categoryReq) {
        return ResponseUtils.ok("Updated", categoryService.updateCategory(categoryId, categoryReq));
    }

    @DeleteMapping("/{categoryId}")
    public ResponseEntity<?> deleteCategory(@PathVariable Integer categoryId) {
        return ResponseUtils.ok("Deleted", categoryService.deleteCategory(categoryId));
    }
}
