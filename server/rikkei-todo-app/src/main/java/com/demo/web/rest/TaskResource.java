package com.demo.web.rest;

import com.demo.service.TaskService;
import com.demo.web.dto.request.TaskReq;
import com.demo.web.dto.response.utils.ResponseUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tasks")
@CrossOrigin("*")
@RequiredArgsConstructor
public class TaskResource {
    private final TaskService taskService;

    @GetMapping
    public ResponseEntity<?> getTaskByCategoryId(@RequestParam(value = "categoryId", required = false) Integer categoryId) {
        return ResponseUtils.ok(taskService.getTasksByCategoryId(categoryId));
    }

    @GetMapping("/{taskId}")
    public ResponseEntity<?> getTaskById(@PathVariable Integer taskId) {
        return ResponseUtils.ok(taskService.getTaskById(taskId));
    }

    @PostMapping
    public ResponseEntity<?> createTask(@RequestBody TaskReq taskReq) {
        return ResponseUtils.created(taskService.createTask(taskReq));
    }

    @PutMapping("/{taskId}")
    public ResponseEntity<?> updateTask(@PathVariable Integer taskId, @RequestBody TaskReq taskReq) {
        return ResponseUtils.ok("Updated", taskService.updateTask(taskId, taskReq));
    }

    @DeleteMapping("/{taskId}")
    public ResponseEntity<?> deleteTask(@PathVariable Integer taskId) {
        return ResponseUtils.ok("Deleted", taskService.deleteTask(taskId));
    }
}
