package com.demo.web.dto;

import lombok.Data;
import org.springframework.lang.Nullable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

@Data
public class TaskDto {
    private Integer id;
    @Nullable
    private Integer categoryId;
    private String title;
    private LocalDate createdDate;
    private LocalDate updatedDate;
    private LocalDate deadline;
    private LocalTime timeAlert;
    private String note;
    private String color;
    private List<AttachFileDto> files;
    private String status;
}
