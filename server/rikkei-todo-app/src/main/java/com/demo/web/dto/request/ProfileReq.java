package com.demo.web.dto.request;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

@Data
public class ProfileReq {
    private String firstName;
    private String lastName;
    private LocalDate dob;
}