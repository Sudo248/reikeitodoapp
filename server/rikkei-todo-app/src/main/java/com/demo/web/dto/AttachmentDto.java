package com.demo.web.dto;

import lombok.Data;

@Data
public class AttachmentDto {
    private String id;
    private String url;
    private String name;
}