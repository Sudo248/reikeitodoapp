package com.demo.service;

import com.demo.web.dto.request.LoginRequest;
import com.demo.web.dto.request.SignupRequest;
import com.demo.web.dto.request.VerifyOTPReq;
import com.demo.web.dto.response.JwtResponse;

public interface AuthService {
    JwtResponse authenticateAccount(LoginRequest loginRequest);

    Integer registerAccount(SignupRequest signupRequest);

    JwtResponse verifyOTP(VerifyOTPReq verifyOTPReq);
}
