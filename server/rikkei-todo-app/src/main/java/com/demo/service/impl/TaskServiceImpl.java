package com.demo.service.impl;

import com.demo.model.AttachFile;
import com.demo.model.Category;
import com.demo.model.Task;
import com.demo.model.constant.TaskStatusConstants;
import com.demo.repository.CategoryRepository;
import com.demo.repository.TaskRepository;
import com.demo.service.TaskService;
import com.demo.service.utils.MappingHelper;
import com.demo.web.dto.AttachFileDto;
import com.demo.web.dto.TaskDto;
import com.demo.web.dto.request.TaskReq;
import com.demo.web.exception.EntityNotFoundException;
import com.demo.web.exception.ServiceException;
import com.demo.web.security.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final CategoryRepository categoryRepository;
    private final MappingHelper mappingHelper;
    private final SecurityUtils securityUtils;

    @Override
    public List<TaskDto> getAllTask() {
        return taskRepository.findAllByAccount(securityUtils.getCurrentAccountLogin())
                .stream().map(this::toTaskDto).collect(Collectors.toList());
    }

    @Override
    public List<TaskDto> getTasksByCategoryId(Integer categoryId) {
        if (categoryId == null || categoryId == -1) {
            return taskRepository.findAllByAccount(securityUtils.getCurrentAccountLogin()).stream()
                    .filter((e) -> {
                        if (e.getCategory() == null) return true;
                        else return e.getCategory().getIsShow();
                    })
                    .map(this::toTaskDto)
                    .collect(Collectors.toList());
        }
        return taskRepository.findByCategory_Id(categoryId)
                .orElseThrow(() -> new EntityNotFoundException(Category.class.getName(), categoryId.toString()))
                .stream().map(this::toTaskDto).collect(Collectors.toList());
    }

    @Override
    public TaskDto getTaskById(Integer taskId) {
        return taskRepository.findById(taskId)
                .map(this::toTaskDto)
                .orElseThrow(() -> new EntityNotFoundException(Task.class.getName(), taskId.toString()));
    }

    @Override
    public TaskDto createTask(TaskReq taskReq) {
        var category = getCategoryOrNull(taskReq.getCategoryId());
        if (taskReq.getStatus() != null && !TaskStatusConstants.STATUS_LIST.contains(taskReq.getStatus().toUpperCase())) {
            throw new ServiceException("Task status must be in the following list: [" + TaskStatusConstants.STATUS_LIST_STRING + "]"
                    , "task-status-error");
        }
        var task = mappingHelper.map(taskReq, Task.class);
        task.setCategory(category);
        task.setCreatedDate(LocalDate.now());
        task.setUpdatedDate(LocalDate.now());
        task.setAccount(securityUtils.getCurrentAccountLogin());
        taskRepository.save(task);
        return toTaskDto(task);
    }

    @Override
    public TaskDto updateTask(Integer taskId, TaskReq taskReq) {
        var task = taskRepository.findById(taskId)
                .map(e -> {
                    mappingHelper.map(taskReq, e);
                    return e;
                })
                .orElseThrow(() -> new EntityNotFoundException(Task.class.getName(), taskId.toString()));
        if (taskReq.getStatus() != null && !TaskStatusConstants.STATUS_LIST.contains(taskReq.getStatus().toUpperCase())) {
            throw new ServiceException("Task status must be in the following list: [" + TaskStatusConstants.STATUS_LIST_STRING + "]"
                    , "task-status-error");
        }
        if (taskReq.getCategoryId() != null) {
            var category = categoryRepository.findById(taskReq.getCategoryId()).orElse(null);
            task.setCategory(category);
        }
        task.setUpdatedDate(LocalDate.now());
        taskRepository.save(task);
        return toTaskDto(task);
    }

    @Override
    public Integer deleteTask(Integer taskId) {
        taskRepository.delete(
                taskRepository.findById(taskId)
                        .orElseThrow(() -> new EntityNotFoundException(Task.class.getName()
                                , taskId.toString())));

        return taskId;
    }

    private TaskDto toTaskDto(Task task) {
        var res = mappingHelper.map(task, TaskDto.class);
        if (task.getFiles() != null) {
            res.setFiles(task.getFiles().stream().map(this::toAttachFileDto)
                    .collect(Collectors.toList()));
        }
        if (task.getCategory() != null) {
            res.setCategoryId(task.getCategory().getId());
        }
        return res;
    }

    private AttachFileDto toAttachFileDto(AttachFile attachFile) {
        return mappingHelper.map(attachFile, AttachFileDto.class);
    }

    private Category getCategoryOrNull(Integer categoryId) {
        if (categoryId == null) return null;
        return categoryRepository.findById(categoryId).orElse(null);
    }
}
