package com.demo.service.impl;

import com.demo.model.Account;
import com.demo.model.EmailDetails;
import com.demo.model.OTPVerifyObject;
import com.demo.repository.AccountRepository;
import com.demo.repository.OTPVerifyObjectRepository;
import com.demo.service.AuthService;
import com.demo.service.utils.EmailHelper;
import com.demo.web.dto.request.LoginRequest;
import com.demo.web.dto.request.SignupRequest;
import com.demo.web.dto.request.VerifyOTPReq;
import com.demo.web.dto.response.JwtResponse;
import com.demo.web.exception.ServiceException;
import com.demo.web.security.jwt.JwtUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Random;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    @Value("${otp.verify.expiration}")
    private Integer otpExpiration;

    private final AccountRepository accountRepository;
    private final AuthenticationManager authenticationManager;
    private final OTPVerifyObjectRepository otpVerifyObjectRepository;
    private final JwtUtils jwtUtils;
    private final PasswordEncoder passwordEncoder;
    private final EmailHelper emailHelper;

    @Override
    public JwtResponse authenticateAccount(LoginRequest loginRequest) {

        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword())
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwtToken = jwtUtils.generateJwtToken(authentication);

            UserDetails userDetails = (UserDetails) authentication.getPrincipal();

            return new JwtResponse(jwtToken, "Bearer", userDetails.getUsername());

        } catch (AuthenticationException authenticationException) {
            throw new ServiceException("Email or password is invalid", "err.authorize.unauthorized");
        }

    }

    @Override
    public Integer registerAccount(SignupRequest signupRequest) {
        accountRepository.findByEmail(signupRequest.getEmail())
                .ifPresent(e -> {
                    throw new ServiceException("Email '" + e.getEmail() + "' is existed in system"
                            , "err.api.email-is-existed");
                });

        var otpObject = otpVerifyObjectRepository.findByEmail(signupRequest.getEmail())
                .orElse(new OTPVerifyObject());
        int otp = new Random().nextInt(9999);
        if (otp < 1000) otp += 1000;
        String message = "Mã xác minh (OTP) của bạn là " + otp + ", có hiệu lực trong vòng 2 phút.";

        Timestamp timeCreateOTP = emailHelper.sendSimpleMail(
                EmailDetails.builder()
                        .subject("Mã xác minh (OTP)")
                        .msgBody(message)
                        .recipient(signupRequest.getEmail())
                        .build());

        otpObject.setEmail(signupRequest.getEmail());
        otpObject.setPassword(passwordEncoder.encode(signupRequest.getPassword()));
        otpObject.setOtp(otp);
        otpObject.setCreatedTime(timeCreateOTP);

        otpVerifyObjectRepository.save(otpObject);
        return otpObject.getId();
    }

    @Override
    public JwtResponse verifyOTP(VerifyOTPReq verifyOTPReq) {
        Timestamp verifyTime = new Timestamp(System.currentTimeMillis());
        var otpObject = otpVerifyObjectRepository.findByEmail(verifyOTPReq.getEmail())
                .orElseThrow(() -> new ServiceException("Email not exist to verify otp", "err.email-not-exist-otp"));
        Timestamp verifyExpiration = new Timestamp(otpObject.getCreatedTime().getTime() + otpExpiration);
        if (verifyOTPReq.getOtp().equals(otpObject.getOtp()) && verifyTime.before(verifyExpiration)) {
            Account account = new Account();
            account.setEmail(otpObject.getEmail());
            account.setPassword(otpObject.getPassword());
            accountRepository.save(account);
            String jwtToken = jwtUtils.generateJwtToken(verifyOTPReq.getEmail());
            return new JwtResponse(jwtToken, "Bearer", verifyOTPReq.getEmail());
        } else throw new ServiceException("OTP code '" + verifyOTPReq.getOtp() + "' is not valid or expired"
                , "err.otp-not-valid-or-expired");
    }
}
