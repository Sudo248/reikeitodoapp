package com.demo.service.impl;

import com.demo.model.Profile;
import com.demo.repository.ProfileRepository;
import com.demo.service.ProfileService;
import com.demo.service.utils.MappingHelper;
import com.demo.web.dto.ProfileDto;
import com.demo.web.dto.request.ProfileReq;
import com.demo.web.exception.ServiceException;
import com.demo.web.security.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProfileServiceImpl implements ProfileService {
    private final ProfileRepository profileRepository;
    private final MappingHelper mappingHelper;
    private final SecurityUtils securityUtils;

    @Override
    public ProfileDto createProfile(ProfileReq profileReq) {
        var profile = mappingHelper.map(profileReq, Profile.class);
        profile.setAccount(securityUtils.getCurrentAccountLogin());
        profileRepository.save(profile);
        return mappingHelper.map(profile, ProfileDto.class);
    }

    @Override
    public ProfileDto updateProfile(ProfileReq profileReq) {
        var account = securityUtils.getCurrentAccountLogin();
        var updatedProfile = profileRepository.findByAccount(account)
                .map(profile -> {
                    mappingHelper.mapIfSourceNotNullAndStringNotBlank(profileReq, profile);
                    profileRepository.save(profile);
                    return profile;
                })
                .orElseThrow(() -> new ServiceException("Account with email '" + account.getEmail()
                        + "' cannot update profile"
                        , "err.account-not-have-profile"));
        return mappingHelper.map(updatedProfile, ProfileDto.class);
    }

    @Override
    public ProfileDto getProfile() {
        var account = securityUtils.getCurrentAccountLogin();
        return profileRepository.findByAccount(account)
                .map(profile -> {
                    var profileDto = mappingHelper.map(profile, ProfileDto.class);
                    profileDto.setEmail(account.getEmail());
                    return profileDto;
                })
                .orElseThrow(() -> new ServiceException("Account with email '" + account.getEmail()
                        + "' cannot update profile"
                        , "err.account-not-have-profile"));
    }
}
