package com.demo.service.impl;

import com.demo.model.Category;
import com.demo.repository.CategoryRepository;
import com.demo.repository.TaskRepository;
import com.demo.service.CategoryService;
import com.demo.service.utils.MappingHelper;
import com.demo.web.dto.CategoryDto;
import com.demo.web.dto.request.CategoryReq;
import com.demo.web.exception.EntityNotFoundException;
import com.demo.web.exception.ServiceException;
import com.demo.web.security.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final TaskRepository taskRepository;
    private final SecurityUtils securityUtils;
    private final MappingHelper mappingHelper;

    @Override
    public List<CategoryDto> getAllCategory(boolean needCategoryAll) {
        List<CategoryDto> allCategory = categoryRepository
                .findAllByCreatedAccount(securityUtils.getCurrentAccountLogin())
                .stream().map(this::toCategoryDto)
                .collect(Collectors.toList());
        if (needCategoryAll) {
            allCategory.add(0, getCategoryAllDto());
        }
        return allCategory;
    }

    @Override
    public CategoryDto getCategoryById(Integer categoryId) {
        return categoryRepository.findById(categoryId)
                .map(this::toCategoryDto)
                .orElseThrow(() -> new EntityNotFoundException(Category.class.getName()
                        , categoryId.toString()));
    }

    @Override
    public CategoryDto createCategory(CategoryReq categoryReq) {
        categoryRepository.findByTitle(categoryReq.getTitle().trim())
                .ifPresent(e -> {
                    throw new ServiceException("category with title '" + e.getTitle() + "' is existed"
                            , "category-existed");
                });
        var category = mappingHelper.map(categoryReq, Category.class);
        category.setCreatedAccount(securityUtils.getCurrentAccountLogin());
        categoryRepository.save(category);
        return mappingHelper.map(category, CategoryDto.class);
    }

    @Override
    public CategoryDto updateCategory(Integer categoryId, CategoryReq categoryReq) {
        var category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new EntityNotFoundException(Category.class.getName(), categoryId.toString()));

        mappingHelper.map(categoryReq, category);
        categoryRepository.save(category);
        return toCategoryDto(category);
    }

    @Override
    public Integer deleteCategory(Integer categoryId) {
        taskRepository.findByCategory_Id(categoryId).ifPresent(taskRepository::deleteAll);
        categoryRepository.deleteById(categoryId);
        return categoryId;
    }

    private CategoryDto getCategoryAllDto() {
        return new CategoryDto(-1, "Tất cả", "#739FED", true, 0);
    }

    private CategoryDto toCategoryDto(Category category) {
        var categoryDto = mappingHelper.map(category, CategoryDto.class);
        categoryDto.setTotalTask(taskRepository.countByCategoryId(category.getId()));
        return categoryDto;
    }
}
