package com.demo.service.impl;

import com.demo.model.AttachFile;
import com.demo.model.Task;
import com.demo.repository.AttachFileRepository;
import com.demo.repository.TaskRepository;
import com.demo.service.AttachmentsService;
import com.demo.service.utils.MappingHelper;
import com.demo.web.dto.AttachFileDto;
import com.demo.web.exception.EntityNotFoundException;
import com.demo.web.rest.AttachmentsResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@Slf4j
@RequiredArgsConstructor
public class AttachmentsServiceImpl implements AttachmentsService {
    private final AttachFileRepository attachFileRepository;
    private final TaskRepository taskRepository;
    private final MappingHelper mappingHelper;

    private final Path root = Paths.get("src/main/resources/attachments");

    @Override
    public AttachFileDto uploadFile(Integer taskId, MultipartFile file) {

        var task = taskRepository.findById(taskId)
                .orElseThrow(() -> new EntityNotFoundException(Task.class.getName(), taskId.toString()));
        AttachFile attachFile = new AttachFile();
        attachFile.setTask(task);
        attachFile.setName(file.getOriginalFilename());
        attachFileRepository.save(attachFile);
        String url = MvcUriComponentsBuilder
                .fromMethodName(AttachmentsResource.class, "getFile", attachFile.getId()
                        , attachFile.getName()).build().toString();
        attachFile.setUrl(url);
        attachFileRepository.save(attachFile);
        try {
            Path directory = this.root.resolve(taskId + "/" + attachFile.getId());
            Files.createDirectories(directory);
            Files.copy(file.getInputStream(), directory.resolve(attachFile.getName()));
        } catch (IOException e) {
            log.error("ERROR", e);
        }
        return mappingHelper.map(attachFile, AttachFileDto.class);
    }

    @Override
    public Resource load(String fileId, String fileName) {
        try {
            var attachFile = attachFileRepository
                    .findById(fileId)
                    .orElseThrow(() -> new EntityNotFoundException(AttachFile.class.getName(), fileId));

            Path file = root.resolve(attachFile.getTask().getId() + "/" + attachFile.getId() + "/"
                    + attachFile.getName());
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public String deleteAttachmentFile(String fileId) {
        try {
            var attachFile = attachFileRepository
                    .findById(fileId)
                    .orElseThrow(() -> new EntityNotFoundException(AttachFile.class.getName(), fileId));

            Path file = root.resolve(attachFile.getTask().getId() + "/" + attachFile.getId() + "/"
                    + attachFile.getName());
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                Path removeFolder = root.resolve(attachFile.getTask().getId() + "/" + attachFile.getId());
                FileUtils.deleteDirectory(removeFolder.toFile());
                attachFileRepository.delete(attachFile);
                return fileId;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("Error deleting the file: " + e.getMessage());
        }
    }

}
