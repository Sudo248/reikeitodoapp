package com.demo.service;

import com.demo.web.dto.TaskDto;
import com.demo.web.dto.request.TaskReq;

import java.util.List;

public interface TaskService {
    List<TaskDto> getAllTask();

    List<TaskDto> getTasksByCategoryId(Integer categoryId);

    TaskDto getTaskById(Integer taskId);

    TaskDto createTask(TaskReq taskReq);

    TaskDto updateTask(Integer taskId, TaskReq taskReq);

    Integer deleteTask(Integer taskId);
}
