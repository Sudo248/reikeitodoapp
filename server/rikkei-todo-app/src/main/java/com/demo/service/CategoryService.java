package com.demo.service;

import com.demo.web.dto.CategoryDto;
import com.demo.web.dto.request.CategoryReq;

import java.util.List;

public interface CategoryService {
    List<CategoryDto> getAllCategory(boolean needCategoryAll);

    CategoryDto getCategoryById(Integer categoryId);

    CategoryDto createCategory(CategoryReq categoryReq);

    CategoryDto updateCategory(Integer categoryId, CategoryReq categoryReq);

    Integer deleteCategory(Integer categoryId);
}
