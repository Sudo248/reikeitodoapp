package com.demo.service;

import com.demo.web.dto.ProfileDto;
import com.demo.web.dto.request.ProfileReq;

public interface ProfileService {
    ProfileDto createProfile(ProfileReq profileReq);

    ProfileDto updateProfile(ProfileReq profileReq);

    ProfileDto getProfile();
}
