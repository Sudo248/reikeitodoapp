package com.demo.service;

import com.demo.web.dto.AttachFileDto;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface AttachmentsService {
    AttachFileDto uploadFile(Integer taskId, MultipartFile file);

    Resource load(String filename, String fileName);

    String deleteAttachmentFile(String fileId);
}
